

log "qwee1" + "asd1" + 1;

#Test some data types
int add1 = 2 + 1* 2;
int add2 = 3;
float fl1 = 1.22323;
float fl2 = 1.2343333;
string str1 = "str1111";
string str2 = "str22323";
bool ok1 = true;
bool ok2 = false;

#Test assignments
int inres = add1+ add2;
float flres1 = fl1+ add2;
float flres2 = add1+ fl2;
float flres3 = fl1+ fl2;

#Test addition
log "add_int_int: "     + ( add1 + add2*( 2 + add1) + 1 );
log "add_int_float: "   + ( add1 + fl2 );
log "add_float_int: "   + ( fl1  + add2 );
log "add_float_float: " + ( fl1  + fl2 );

#Test subs
log "sub_int_int: "     + (add1 - add2);
log "sub_int_float: "   + (add1 - fl2);
log "sub_float_int: "   + (fl1  - add2);
log "sub_float_float: " + (fl1  - fl2);

#Test mult
log "mult_int_int: "     + (add1 * add2);
log "mult_int_float: "   + (add1 * fl2);
log "mult_float_int: "   + (fl1  * add2);
log "mult_float_float: " + (fl1  * fl2);

#Test div
log "div_int_int: "     + (add1 / add2);
log "div_int_float: "   + (add1 / fl2);
log "div_float_int: "   + (fl1  / add2);
log "div_float_float: " + (fl1  / fl2);

#Tets ls
log "greater then int_int: " + (add1>add2);
log "greater then int_int: " + (1>add2);
log "greater then fl_int: " + (fl1>add2);
log "greater then fl_fl: " + (fl2>fl1);

log "greater then eq int_int: " + (add1>=add2);
log "greater then eq int_int: " + (1>=add2);
log "greater then eq fl_int:" + (fl1>=add2);
log "greater then eq fl_fl:  " + (fl2>=fl1);

log "lesser then int_int: " + (add1<add2);
log "lesser then int_int: " + (1<add2);
log "lesser then fl_int: " + (fl1<add2);
log "lesser then fl_fl:  " + (fl2<fl1);
  
log "lesser then eq int_int: " + (add1<=add2);
log "lesser then eq int_int: " + (1<=add2);
log "lesser then eq fl_int: " + (fl1<=add2);
log "lesser then eq fl_fl:  " + (fl2<=fl1);


#Test while loop
int counter = 0;
while(counter < 3){
	int isls = 2;
	counter = counter + 1;
	if (counter > isls){
		log "xexe";
	}
}

#Test if-else if-else
string strstr1 = "aa";
if (true){
	strstr1 = "qasd";
	if (true && false){
		strstr1 = "rddd";
	}
}else if(false){
	strstr1 =  "asddddw";
} else {
	strstr1 = "asddde";
}
log strstr1;

#Test arrays
int[7] www = 1;
www[0] = 2;
int add233 = www[6];
www[1] = 1;
www[3] = www[4];
log www[6];

#Functions
string returnString(string t, bool ok, int[] inti) {
	int counter1 = counter;
	string aaasss = "sdf";
	string aasss = "sdf";
	while(ok && (counter1 < 6)){
		string aasssd = "sdf";
		counter1 = counter1 + inti[6];
		if (counter1 < 3){
			log "xexe";
		}
	}
	string aasssd = "sdf";
	return "asda";
}
counter = 0;
string stre = returnString("asdad", true, www);
counter = 1;
returnString("asdad", true, www);
