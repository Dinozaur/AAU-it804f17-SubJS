log "float BinO int";
float flf = 1.2;
int iinntt = 1;
log flf/iinntt;
log flf-iinntt;
log flf+iinntt;
log flf*iinntt;
log "int BinO int";
int in2 = 2;
log in2/iinntt;
log in2-iinntt;
log in2+iinntt;
log in2*iinntt;
log "float BinO float";
int fl2 = 2;
log flf/fl2;
log flf-fl2;
log flf+fl2;
log flf*fl2;
log "--------------------";
string returnString(string t, bool ok, int[] inti) {
	#int i;
	int i = 1;
	#i = 0;
	if (ok) {
		while (i < inti[5]){
			t = t + "---" + i + "--- ";
			i = i + 1;
		}	
		return t;
	} else if (true) {
		while (i < 7) {
			log "inti[" + i + "]=" + inti[i];
			i = i + 1;
		}
		return t;
	} else {
		return t;
	}
		return t;
}

int[7] aaa = 1;
aaa[0] = 1;
aaa[1] = 2;
aaa[2] = 3;
aaa[3] = 4;
aaa[4] = 5;
aaa[5] = 6;
aaa[6] = 7;
bool ok1 = true; #1<2 && 2<=3;
string str= "str1";
log "call returnString(str, false, aaa): " + returnString(str, ok1, aaa);
str = "str2";
ok1 = false;
log "call returnString(str, true, aaa): " + returnString(str, ok1, aaa);

int ii = 1;
#example of type checking
#bool ok0 = 1<"str";
#log ok0;
#bool ok2 = 1<=1.0;
#log ok2;
#bool ok3 = ii>"str";
#log ok3;
#bool ok4 = ii<="str";
#log ok4;
#str = "str1";
#ok1 = true;

#str = 2;
#returnString(str, "asdads", aaa);
#aaa[6] = 1 + returnString(str, ok1, aaa);