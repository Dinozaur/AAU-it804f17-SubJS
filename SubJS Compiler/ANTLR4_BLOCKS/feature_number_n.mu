


bool a = true;
bool b = false;
#bool c = 1;
# It is not possible to assign numeric values to booleans.

#int x;
#float y + x;
# also throws an error, undeclared variables are not allowed.

#string x = "99";
#bool bb = true + x;
# throws an error as it should# Lowered orthongonality from JavaScript.

float a1 = 0.1;
float a2 = 0.2;
float a3 = a1 + a2;
if(a3 == 0.3){
	log "yes";
} else {
	log "no";
} 
#problem with using floating points

int WouldReturnTenInJSNoParm(){
  int i = 1;
   if(true) {  
   		int i = 10; 
   		log i;
   	} 
   log i;
   return i;
}

int WouldReturnTenInJS(int ty){
  int i = 1;
   if(true) {  
   		int i = ty; 
   		log i;
   	} 
   log i;
   return i;
}


int WouldReturnTenInJSArray(int[] ty){
  int i = 1;
   if(true) {  
   		int i = ty[0]; 
   		log i;
   	} 
   log i;
   return i;
}


int WouldReturnTenInJSTwoParams(int[] ty, int index){
  int i = 1;
   if(true) {  
   		int i = ty[index]; 
   		log i;
   	} 
   log i;
   return i;
}

int[7] j = 1;
j[4] = 2;
int ji = 10;
WouldReturnTenInJS(12333);
if(true){
	WouldReturnTenInJSNoParm();
}
WouldReturnTenInJSArray(j);
if(true){
	WouldReturnTenInJSTwoParams(j, 4);
}


int ii1 = WouldReturnTenInJS(12333);
if(true){
	int ii2 = WouldReturnTenInJSNoParm();
}
int ii3 = WouldReturnTenInJSArray(j);
if(true){
	int ii4 = WouldReturnTenInJSTwoParams(j, 4);
}


# This will return 1 in SubJS due to variables not being made global like in JavaScript.