package nodes;

public class ConditionBlockNode extends AbstractASTNode {
	private AbstractASTNode expNode;
	private BlockNode blockNode;
	public AbstractASTNode getExpNode() {
		return expNode;
	}
	public void setExpNode(AbstractASTNode expNode) {
		this.expNode = expNode;
	}
	public BlockNode getBlockNode() {
		return blockNode;
	}
	public void setBlockNode(BlockNode blockNode) {
		this.blockNode = blockNode;
	}
}
