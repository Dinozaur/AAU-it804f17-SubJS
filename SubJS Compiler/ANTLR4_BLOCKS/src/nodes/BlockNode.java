package nodes;

import java.util.ArrayList;
import java.util.List;

public class BlockNode extends AbstractASTNode {

	private List<AbstractASTNode> stat = new ArrayList<AbstractASTNode>();
	
	public List<AbstractASTNode> getStat()
	{
		return this.stat;
	}
	
	public AbstractASTNode getStatById(int i)
	{
		return this.stat.get(i);
	}
	
	public void setStat(List<AbstractASTNode> len){
		this.stat = len;
	}
	
	public void addStat(AbstractASTNode en){
		this.stat.add(en);
	}
}
