package nodes;

import java.util.ArrayList;
import java.util.List;

public class CallFunctExprNode extends AbstractASTNode{
	 
		private IdNode IdNode;	
		private List<AbstractASTNode> argList = new ArrayList<AbstractASTNode>();
		
		public IdNode getIdNode() {
			return IdNode;
		}
		public void setIdNode(IdNode idNode) {
			IdNode = idNode;
		}
		public List<AbstractASTNode> getArgList() {
			return argList;
		}
		public void setArgList(List<AbstractASTNode> argList) {
			this.argList = argList;
		}

}
