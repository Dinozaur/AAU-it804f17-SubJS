package nodes;

public class DeclarationNode extends AbstractASTNode {
	 
	private IdNode IdNode;	
	private int nodeType = -1;
	private AbstractASTNode arraySizeExpression;//for arrays x[2+4*3]
	private AbstractASTNode ExprNode;

	public AbstractASTNode getExprNode() {
		return ExprNode;
	}

	public void setExprNode(AbstractASTNode innerNode) {
		ExprNode = innerNode;
	}
	
	public IdNode getIdNode() {
		return IdNode;
	}

	public void setIdNode(IdNode idNode) {
		IdNode = idNode;
	}

	public int getNodeType() {
		return nodeType;
	}

	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}

	public AbstractASTNode getArraySizeExpression() {
		return arraySizeExpression;
	}

	public void setArraySizeExpression(AbstractASTNode arraySizeExpression) {
		this.arraySizeExpression = arraySizeExpression;
	}

}
