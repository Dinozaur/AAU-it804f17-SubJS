package nodes;

import java.util.ArrayList;
import java.util.List;

public class IfStatNode extends AbstractASTNode {
	
	private List<ConditionBlockNode> conblocknodes = new ArrayList<ConditionBlockNode>();
	private BlockNode blockNode; //stat_block
	
	public List<ConditionBlockNode> getConditionBlockNode()
	{
		return this.conblocknodes;
	}
	
	public AbstractASTNode getConditionBlockNodeById(int i)
	{
		return this.conblocknodes.get(i);
	}
	
	public void setConditionBlockNode(List<ConditionBlockNode> len){
		this.conblocknodes = len;
	}
	
	public void addConditionBlockNode(ConditionBlockNode cbn){
		this.conblocknodes.add(cbn);
	}

	public BlockNode getBlockNode() {
		return this.blockNode;
	}

	public void setBln(BlockNode blockNode) {
		this.blockNode = blockNode;
	}
	
}
