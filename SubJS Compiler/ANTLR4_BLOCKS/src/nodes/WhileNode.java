package nodes;

public class WhileNode extends AbstractASTNode {
	
	private AbstractASTNode expressionNode;
	private BlockNode blockNode;	
	
	public BlockNode getBlockNode() {
		return this.blockNode;
	}

	public void setBln(BlockNode blockNode) {
		this.blockNode = blockNode;
	}

	public AbstractASTNode getExpressionNode() {
		return expressionNode;
	}

	public void setExpressionNode(AbstractASTNode expressionNode) {
		this.expressionNode = expressionNode;
	}
	
}
