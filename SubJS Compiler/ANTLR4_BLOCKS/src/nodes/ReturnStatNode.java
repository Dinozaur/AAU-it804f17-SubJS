package nodes;

public class ReturnStatNode extends AbstractASTNode{
	private AbstractASTNode en;

	public AbstractASTNode getEn() {
		return en;
	}

	public void setEn(AbstractASTNode en) {
		this.en = en;
	}
}
