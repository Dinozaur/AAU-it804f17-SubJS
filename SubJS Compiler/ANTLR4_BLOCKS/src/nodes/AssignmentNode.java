package nodes;

public class AssignmentNode extends AbstractASTNode {
	 
	private IdNode IdNode;	
	private AbstractASTNode ArrayIdIndExprNode;
	private AbstractASTNode ExprNode;

	public AbstractASTNode getExprNode() {
		return ExprNode;
	}

	public void setExprNode(AbstractASTNode innerNode) {
		ExprNode = innerNode;
	}

	public IdNode getIdNode() {
		return IdNode;
	}

	public void setIdNode(IdNode idNode) {
		IdNode = idNode;
	}

	public AbstractASTNode getArrayIdIndExprNode() {
		return ArrayIdIndExprNode;
	}

	public void setArrayIdIndExprNode(AbstractASTNode arrayIdIndExprNode) {
		ArrayIdIndExprNode = arrayIdIndExprNode;
	}
}
