package nodes;

import jss.Value;

public class IdAtomNode extends AbstractASTNode {
	private Value val;
	public Value getValue() {
		return this.val;
	}
	public void setValue(Value value) {
		this.val = value;
	}
}
