package nodes;

import jss.Value;

public class IdNode extends AbstractASTNode {
	public IdNode(){

	}
	private Value val;
	
	public Value getValue() {
		return this.val;
	}
	public void setValue(Value value) {
		this.val = value;
	}
}