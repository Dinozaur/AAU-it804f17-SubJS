package nodes;

public  abstract class InfixExpressionNode extends AbstractASTNode{
    private AbstractASTNode Left; 
    private AbstractASTNode Right;
	public AbstractASTNode getLeft() {
		return Left;
	}
	public void setLeft(AbstractASTNode left) {
		Left = left;
	}
	public AbstractASTNode getRight() {
		return Right;
	}
	public void setRight(AbstractASTNode right) {
		Right = right;
	} 
}
