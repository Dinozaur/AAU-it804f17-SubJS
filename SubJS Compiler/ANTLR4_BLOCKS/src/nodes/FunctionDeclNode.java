package nodes;

import java.util.ArrayList;
import java.util.List;

public class FunctionDeclNode extends AbstractASTNode {
	 
	private int nodeType = -1;
	private IdNode IdNode;	
	private AbstractASTNode arraySizeArray;// if type is array
	
	//this are the formalParameters
	private List<DeclarationNode> declrs = new ArrayList<DeclarationNode>();
	
	//stat_block
	private BlockNode blockNode;	

	
	public BlockNode getBlockNode() {
		return this.blockNode;
	}

	public void setBln(BlockNode blockNode) {
		this.blockNode = blockNode;
	}
	
	public IdNode getIdNode() {
		return IdNode;
	}

	public void setIdNode(IdNode idNode) {
		IdNode = idNode;
	}

	public int getNodeType() {
		return nodeType;
	}

	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}

	public AbstractASTNode getArraySizeArray() {
		return arraySizeArray;
	}

	public void setArraySizeArray(AbstractASTNode arraySizeArray) {
		this.arraySizeArray = arraySizeArray;
	}

	public List<DeclarationNode> getDeclrs() {
		return declrs;
	}

	public void setDeclrs(List<DeclarationNode> declrs) {
		this.declrs = declrs;
	}

}
