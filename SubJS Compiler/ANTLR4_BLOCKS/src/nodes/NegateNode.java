package nodes;

public class NegateNode extends AbstractASTNode {
	 private AbstractASTNode InnerNode;

	public AbstractASTNode getInnerNode() {
		return InnerNode;
	}

	public void setInnerNode(AbstractASTNode innerNode) {
		InnerNode = innerNode;
	}
}
