package jss;

import java.util.ArrayList;
import java.util.List;

import nodes.*;

public class StaticCheckingVisitor extends AstVisitor<Value>
{	
    public static final double SMALL_VALUE = 0.00000000001;
	private static final SymtabInterface sti = new TeamSymtab();
	private static boolean DEBUG_MODE = false;
	@Override       
    public Value Visit(BlockNode node) {
		sti.incrNestLevel();
		Value val = null;
    	for(AbstractASTNode en : node.getStat()){
    		
    		val = Visit(en); 
    		if(val != null){
    			 break;
    		}
    	}
    	sti.decrNestLevel();
        return val;
    }
	
	@Override
	 public Value Visit(AssignmentNode node){
		Value id = Visit(node.getIdNode());
		Value rhs = Visit(node.getExprNode());

		if(sti.contObject(id.asString())){
			Value lhs = sti.lookup(id.asString());
			 //Not sure about the semantics modify after
			if(lhs.type == rhs.type){
				if(node.getArrayIdIndExprNode() != null && Visit(node.getArrayIdIndExprNode()).asInteger() != -1){
					Value valArrIndexExpress = Visit(node.getArrayIdIndExprNode());	
					Value vva = new Value(null);
					switch (lhs.type) {
					case SubJSLexer.TYPE_INT:
						int[] arr1 = (int[])lhs.value;
						arr1[valArrIndexExpress.asInteger()] =  (int) rhs.value ;
						vva = new Value(arr1);
						vva.type = lhs.type;
						vva.setArray(true);
						sti.enter(id.asString(), vva);
						break;
					case SubJSLexer.TYPE_FLOAT:
						double[] arr2 = (double[])lhs.value;
						arr2[valArrIndexExpress.asInteger()] =  (double) rhs.value ;
						vva = new Value(arr2);
						vva.type = lhs.type;
						vva.setArray(true);
						sti.enter(id.asString(), vva);
						break;
					case SubJSLexer.TYPE_STRING:
						String[] arr3 = (String[])lhs.value;
						arr3[valArrIndexExpress.asInteger()] =  (String) rhs.value ;
						vva = new Value(arr3);
						vva.type = lhs.type;
						vva.setArray(true);
						sti.enter(id.asString(), vva);
						break;
					case SubJSLexer.TYPE_BOOLEAN:
						Boolean[] arr4 = (Boolean[])lhs.value;
						arr4[valArrIndexExpress.asInteger()] =  (Boolean) rhs.value ;
						vva = new Value(arr4);
						vva.type = lhs.type;
						vva.setArray(true);
						sti.enter(id.asString(), vva);
						break;
					default:
						return null;
			        }
					
				} else {
					sti.enter(id.asString(), rhs);
				}
			}else{
				//add more info about the error
	        	throw new RuntimeException("\n\nFaild to assign value of type " + SubJSLexer.ruleNames[rhs.type - 1]
	        			+ " to variable " + id.asString()
	        			+ " which has type " + SubJSLexer.ruleNames[sti.lookup(id.asString()).type - 1]);
			}
		}else{
        	throw new RuntimeException("\nVariable '"  + id.asString() +  "' not declared.");
		}
		return null;//value
	 }
	
	@Override
    public Value Visit(AdditionNode node) {
        Value left = Visit(node.getLeft());
        Value right = Visit(node.getRight()); 
        Value val = null;
		 
 
        	 if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_INT){
    			 val = new Value(left.asInteger()  + right.asInteger());
    			 val.type = SubJSLexer.TYPE_INT;
    		 } else if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_FLOAT){
    			 val = new Value(left.asInteger()  +  right.asDouble());
    			 val.type = SubJSLexer.TYPE_FLOAT;
    		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
    			 val = new Value(left.asDouble()  +  right.asInteger());
    			 val.type = SubJSLexer.TYPE_FLOAT;
    		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
    			 val = new Value(left.asDouble()  +  right.asDouble());
    			 val.type = SubJSLexer.TYPE_FLOAT;
    		 } else if(left.type == SubJSLexer.TYPE_STRING || right.type == SubJSLexer.TYPE_STRING){
    			 val = new Value(left.asString() + right.asString());
    			 val.type = SubJSLexer.TYPE_STRING;
    		 } else {
    				throw new RuntimeException("\nType mismatch in  - operation. Left operant " 
    	        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
    	        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
    			 
    		 }
		return val;
    }
	
	@Override
    public Value Visit(SubtractionNode node){
        Value left = Visit(node.getLeft());
        Value right = Visit(node.getRight());
        Value val = null;
		 //Not sure about the semantics modify after
        if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_INT){
			 val = new Value(left.asInteger()  - right.asInteger());
			 val.type = SubJSLexer.TYPE_INT;
		 } else if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_FLOAT){
			 val = new Value(left.asInteger()  -  right.asDouble());
			 val.type = SubJSLexer.TYPE_FLOAT;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
			 val = new Value(left.asDouble()  -  right.asInteger());
			 val.type = SubJSLexer.TYPE_FLOAT;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
			 val = new Value(left.asDouble()  -  right.asDouble());
			 val.type = SubJSLexer.TYPE_FLOAT;
		 } else {
	           	throw new RuntimeException("\nType mismatch in  - operation. Left operant " 
	        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
	        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
	        }
		return val;
    }
	
	@Override
    public Value Visit(MultiplicationNode node){
        Value left = Visit(node.getLeft());
        Value right = Visit(node.getRight());
        Value val = null;
		 //Not sure about the semantics modify after
		 if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_INT){
			 val = new Value(left.asInteger()  * right.asInteger());
			 val.type = SubJSLexer.TYPE_INT;
		 } else if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_FLOAT){
			 val = new Value(left.asInteger()  *  right.asDouble());
			 val.type = SubJSLexer.TYPE_FLOAT;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
			 val = new Value(left.asDouble()  *  right.asInteger());
			 val.type = SubJSLexer.TYPE_FLOAT;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
			 val = new Value(left.asDouble()  *  right.asDouble());
			 val.type = SubJSLexer.TYPE_FLOAT;
		 } else {
	           	throw new RuntimeException("\nType mismatch in  * operation. Left operant " 
	        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
	        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
	        }
		return val;
    }
	
	@Override
    public Value Visit(DivisionNode node) {
        Value left = Visit(node.getLeft());
        Value right = Visit(node.getRight());

        Value val = null;
		 //Not sure about the semantics modify after
		 if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
		        if(right.asInteger() == 0){
		        	throw new RuntimeException("\nDenominator can not be 0");
		        }
			 val = new Value(left.asInteger()  / right.asInteger());
			 val.type = SubJSLexer.TYPE_INT;
		 } else if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_FLOAT){
		        if(right.asDouble() == 0){
		        	throw new RuntimeException("\nDenominator can not be 0");
		        }
			 val = new Value(left.asInteger()  /  right.asDouble());
			 val.type = SubJSLexer.TYPE_FLOAT;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
		        if(right.asInteger() == 0){
		        	throw new RuntimeException("\nDenominator can not be 0");
		        }
			 val = new Value(left.asDouble()  /  right.asInteger());
			 val.type = SubJSLexer.TYPE_FLOAT;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
		        if(right.asDouble() == 0){
		        	throw new RuntimeException("\nDenominator can not be 0");
		        }
			 val = new Value(left.asDouble()  /  right.asDouble());
			 val.type = SubJSLexer.TYPE_FLOAT;
		 } else {
	           	throw new RuntimeException("\nType mismatch in  / operation. Left operant " 
	        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
	        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
	        }
		 
		return val;
    }
	
	@Override
    public Value Visit(AndNode node) {
        Value left = Visit(node.getLeft());
        Value right = Visit(node.getRight());
        Value result = null;
		 //Not sure about the semantics modify after
        if(left.type == right.type ){
            if(left.type == SubJSLexer.TYPE_BOOLEAN ){
            	result = new Value(left.asBoolean() && right.asBoolean());
            }else{
            	throw new RuntimeException("\nAnd operation defined only for BOOLEAN");
            }
        	result.type = left.type;
        }else{
           	throw new RuntimeException("\nType mismatch in And operation. Left operant " 
        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type - 1]);
        }
		return result;
    }
	
	@Override
    public Value Visit(OrNode node) {
        Value left = Visit(node.getLeft());
        Value right = Visit(node.getRight());
        Value result = null;
		 //Not sure about the semantics modify after
        if(left.type == right.type ){
            if(left.type == SubJSLexer.TYPE_BOOLEAN ){
            	result = new  Value(left.asBoolean() || right.asBoolean());
            }else{
            	throw new RuntimeException("\nOr operation defined only for BOOLEAN");
            }
        	result.type = left.type;
        }else{
           	throw new RuntimeException("\nType mismatch in Or operation. Left operant " 
        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type - 1]);
        }
		return result;
    }
	
	@Override
    public  Value Visit(NegateNode node){
		 Value value = Visit(node.getInnerNode());
	     Value result = null;
		 //Not sure about the semantics modify after
	     if(value.type == SubJSLexer.TYPE_INT){
	     	result = new Value(-value.asInteger());
	     }else if(value.type == SubJSLexer.TYPE_FLOAT) {
	    	result = new Value(-value.asDouble()); 
	     }else{
	     	throw new RuntimeException("\nMultiplication operation defined only for INT and FLOAT");
	     }
	     result.type = value.type;
		return result;
    }
	
	@Override
    public  Value Visit(ParentExprNode node){
		 Value value = Visit(node.getInnerNode());
		return value;
    }
	
	@Override
    public Value Visit(NumberNode node) {
        return node.getValue();
    }
	
	@Override
    public Value Visit(IdNode node) {
        return node.getValue();
    }

	@Override
	public Value Visit(PrintNode node) {
		Value value = Visit(node.getInnerNode());
		if(isDEBUG_MODE()){
			System.out.println(value);
		}
		return null;
	}
	
	@Override
    public Value Visit(IdAtomNode node) {
        return sti.lookup(node.getValue().asString());
    }
	
	@Override
	public Value Visit(IfStatNode node) {
		Boolean evaluatedBlock = false;
		Value res = null;
		for(ConditionBlockNode en : node.getConditionBlockNode()){
			Value valuexp = Visit(en.getExpNode());
			if(valuexp.asBoolean()){
				res = Visit(en.getBlockNode());
				evaluatedBlock = true;
				return res;
			}
		}
		if(!evaluatedBlock &&  node.getBlockNode() != null){
			res = Visit(node.getBlockNode());
			if(res != null){
				evaluatedBlock = true;
				return res;
			}
		}
		return res;
	}
	
	@Override
	public Value Visit(WhileNode node) {
		  Value value = Visit(node.getExpressionNode());
		  Value res = null;
		  //Chack whether value is boolean, maybe also at the end
		  if(value.type == SubJSLexer.TYPE_BOOLEAN){
	        while(value.asBoolean()) {
	            // evaluate the code block
	        	res = Visit(node.getBlockNode());
	        	if(res != null){
	        		break;
	        	}
	            // evaluate the expression
	            value = Visit(node.getExpressionNode());
	        }
		  }else{
				throw new RuntimeException("\nError while loop expression is not an boolean type.");
		  }
	        return res;
	}
	
	@Override
	public Value Visit(ConditionBlockNode node) {
		Value valuexp = Visit(node.getExpNode());
		if(valuexp.asBoolean()){
			 Visit(node.getBlockNode());
		}
        return new Value(valuexp.asBoolean());
	}

	@Override
	public Value Visit(GTEQNode node) {
		 Value left = Visit(node.getLeft());
		 Value right = Visit(node.getRight());
		 Value val = null;
		 //Not sure about the semantics modify after
		 if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
			 val = new Value(left.asInteger()  >= right.asInteger());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_FLOAT){
			 val = new Value(left.asInteger()  >=  right.asDouble());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
			 val = new Value(left.asDouble()  >= right.asInteger());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
			 val = new Value(left.asDouble()  >=  right.asDouble());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else {
	           	throw new RuntimeException("\nType mismatch in  >= operation. Left operant " 
	        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
	        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
	        }
      return val;
	}
	
	@Override
	public Value Visit(LTEQNode node) {
		 Value left = Visit(node.getLeft());
		 Value right = Visit(node.getRight());
		 Value val = null;
		 
		 if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
			 val = new Value(left.asInteger()  <=  right.asInteger());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_FLOAT){
			 val = new Value(left.asInteger()  <=  right.asDouble());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
			 val = new Value(left.asDouble()  <=  right.asInteger());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
			 val = new Value(left.asDouble()  <=  right.asDouble());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else {
	           	throw new RuntimeException("\nType mismatch in  <= operation. Left operant " 
	        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
	        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
	        }

       return val;
	}
	
	@Override
	public Value Visit(GTNode node) {
		 Value left = Visit(node.getLeft());
		 Value right = Visit(node.getRight());
		 Value val = null;
		 
		 if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
			 val = new Value(left.asInteger()  >  right.asInteger());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_FLOAT){
			 val = new Value(left.asInteger()  >  right.asDouble());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
			 val = new Value(left.asDouble()  >  right.asInteger());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT && right.type == SubJSLexer.TYPE_FLOAT){
			 val = new Value(left.asDouble()  >  right.asDouble());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else {
	           	throw new RuntimeException("\nType mismatch in  > operation. Left operant " 
	        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
	        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
	        }

       return val;  
	}
	
	@Override
	public Value Visit(LTNode node) {
		 Value left = Visit(node.getLeft());
		 Value right = Visit(node.getRight());
 
		 Value val = null;
		 
		 if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
			 val = new Value(left.asInteger()  <  right.asInteger());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_FLOAT){
			 val = new Value(left.asInteger()  <  right.asDouble());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
			 val = new Value(left.asDouble()  <  right.asInteger());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
			 val = new Value(left.asDouble()  <  right.asDouble());
			 val.type = SubJSLexer.TYPE_BOOLEAN;
		 } else {
	           	throw new RuntimeException("\nType mismatch in  < operation. Left operant " 
	        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
	        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
	        }
        return val;
	}
	
	@Override
	public Value Visit(EQNode node) {
        Value left = Visit(node.getLeft());
        Value right = Visit(node.getRight());
		 //Not sure about the semantics modify after
        
        if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
        	Value val = new Value(Math.abs(left.asInteger() - right.asInteger()) < SMALL_VALUE);
        	val.type = SubJSLexer.TYPE_BOOLEAN;
        	return  val;
		 } else if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_FLOAT){
			 Value val = new Value(Math.abs(left.asInteger() - right.asDouble()) < SMALL_VALUE);
	        	val.type = SubJSLexer.TYPE_BOOLEAN;
	        	return  val;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
			 Value val = new Value(Math.abs(left.asDouble() - right.asInteger()) < SMALL_VALUE);
	        	val.type = SubJSLexer.TYPE_BOOLEAN;
	        	return  val;
		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
			 Value val = new Value(Math.abs(left.asDouble() - right.asDouble()) < SMALL_VALUE);
	        	val.type = SubJSLexer.TYPE_BOOLEAN;
	        	return  val;
		 } else if(left.type == SubJSLexer.TYPE_BOOLEAN && right.type == SubJSLexer.TYPE_BOOLEAN) {
        	Value val = new Value(left.asBoolean().equals(right.asBoolean()));
        	val.type = SubJSLexer.TYPE_BOOLEAN;
        	return val;
        } else if(left.type == SubJSLexer.TYPE_STRING && right.type == SubJSLexer.TYPE_STRING ) {
        	Value val = new Value(left.asString().equals(right.asString()));
        	val.type = SubJSLexer.TYPE_BOOLEAN;
        	return val;
        } else {
        	throw new RuntimeException("\nType mismatch in EQ(=) operation. Left operant " 
        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type - 1]);
        }
	}
	
	@Override
	public Value Visit(NEQNode node) {
        Value left = Visit(node.getLeft());
        Value right = Visit(node.getRight()); 
		 //Not sure about the semantics modify after
        if(left.isDouble() && right.isDouble()){
        	Value val = new Value(Math.abs(left.asDouble() - right.asDouble())  >= SMALL_VALUE);
        	val.type = SubJSLexer.TYPE_BOOLEAN;
        	return  val; 	
        } else if(left.isBoolean() && right.isBoolean()) {
        	Value val = new Value(!left.asBoolean().equals(right.asBoolean()));
        	val.type = SubJSLexer.TYPE_BOOLEAN;
        	return val;
        } else if(left.isString() && right.isString()) {
        	Value val = new Value(!left.asString().equals(right.asString()));
        	val.type = SubJSLexer.TYPE_BOOLEAN;
        	return val;
        } else {
        	throw new RuntimeException("\nType mismatch in NEQ(!=) operation. Left operant " 
        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type - 1]);
        } 
	}
	
	@Override
	public Value Visit(DeclarationNode node) {
		//Need to check if variable is already defined
		node.getIdNode().getValue().type = node.getNodeType();
		Value v = null;

		if(node.getArraySizeExpression() == null || Visit(node.getArraySizeExpression()).asInteger() == -1){
			if(node.getExprNode() != null){
				v = Visit(node.getExprNode());
			}
			else{
				v = new Value(null);
			}
			v.setArray(false);
		} else{

			Value arrSize = Visit(node.getArraySizeExpression());
			int size = arrSize.asInteger();
			
			switch (node.getNodeType()) {
			case SubJSLexer.TYPE_INT:
				int[] arr1 = new int[size];
				for(int i = 0; i < size; i++){
					arr1[i] =   Visit(node.getExprNode()).asInteger();
				}
				v = new Value(arr1);
				break;
			case SubJSLexer.TYPE_FLOAT:
				double[] arr2 = new double[size];
				for(int i = 0; i < size; i++){
					arr2[i] =   Visit(node.getExprNode()).asDouble();
				}
				break;
			case SubJSLexer.TYPE_STRING:
				String[] arr3 = new String[size];
				for(int i = 0; i < size; i++){
					arr3[i] =   Visit(node.getExprNode()).asString();
				}
				break;
			case SubJSLexer.TYPE_BOOLEAN:
				Boolean[] arr4 = new Boolean[size];
				for(int i = 0; i < size; i++){
					arr4[i] =   Visit(node.getExprNode()).asBoolean();
				}
				break;
			default:
				return null;
	        }
			v.setArray(true);
		}
		v.type = node.getNodeType();
		sti.enter(node.getIdNode().getValue().asString(), v);
		//Need to add the type possible new type
		return null;
	}
	
	@Override
	public Value Visit(ArrayExprNode node) {
		Value vr = Visit(node.getLeft());
		Value ind = Visit(node.getRight());
		Value tempArray = sti.lookup(vr.asString());
		
		Value val = null;
	
		switch (tempArray.type) {
		case SubJSLexer.TYPE_INT:
			int tempElement1 = ((int[])tempArray.value )[ind.asInteger()];
			val = new Value(tempElement1);
			break;
		case SubJSLexer.TYPE_FLOAT:
			double tempElement2 = ((double[])tempArray.value )[ind.asInteger()];
			val = new Value(tempElement2);
			break;
		case SubJSLexer.TYPE_STRING:
			String tempElement3 = ((String[])tempArray.value )[ind.asInteger()];
			val = new Value(tempElement3);
			break;
		case SubJSLexer.TYPE_BOOLEAN:
			Boolean tempElement4 = ((Boolean[])tempArray.value )[ind.asInteger()];
			val = new Value(tempElement4);
			break;
		default:
			return null;
        }
		
		val.type = tempArray.type;
		val.setArray(false);
		return val;
		
	}

	@Override
	public Value Visit(FunctionDeclNode node) {
		Value ind = Visit(node.getIdNode());
		Value result = new Value(node);
		result.type = node.getNodeType();
		sti.enter(ind.asString(), result);
		return null;
	}

	@Override
	public Value Visit(CallFunctExprNode node) {
		Value id = Visit(node.getIdNode());
		FunctionDeclNode fdn = (FunctionDeclNode)sti.lookup(id.asString()).value;
		List<AbstractASTNode> temppListDecl = new ArrayList<AbstractASTNode>();
		List<AbstractASTNode> temppListAsign = new ArrayList<AbstractASTNode>();
		for(int i = 0; i < fdn.getDeclrs().size(); i++){
			DeclarationNode dn =  fdn.getDeclrs().get(i);
			AssignmentNode an = new AssignmentNode();
			IdNode idn = dn.getIdNode();
			AbstractASTNode nn = node.getArgList().get(i);
			if(dn.getArraySizeExpression() != null){	
				an.setArrayIdIndExprNode(dn.getArraySizeExpression());
			}
			an.setIdNode(idn);
			an.setExprNode(nn);
			temppListDecl.add(dn);
			temppListAsign.add( an);
		}
		fdn.getBlockNode().getStat().addAll(0, temppListDecl);
		fdn.getBlockNode().getStat().addAll(temppListDecl.size(), temppListAsign);
		Value result = Visit(fdn.getBlockNode());
		result.type = fdn.getNodeType();
		fdn.getBlockNode().getStat().removeAll(temppListDecl);
		fdn.getBlockNode().getStat().removeAll(temppListAsign);
		return result;
	}

	@Override
	public Value Visit(FunctionStatNode node) {
		Visit(node.getCfen());
		return null;
	}

	public static void setDEBUG_MODE(boolean dEBUG_MODE) {
		DEBUG_MODE = dEBUG_MODE;
	}

	@Override
	public Value Visit(ReturnStatNode node) {
		// TODO Auto-generated method stub
		return Visit(node.getEn());
	}
	
	public static boolean isDEBUG_MODE() {
		return DEBUG_MODE;
	}

	
	
}
