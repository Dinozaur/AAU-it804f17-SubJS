package jss;

import nodes.*;

abstract class AstVisitor<T>
{
    public abstract T Visit(AdditionNode node);
    public abstract T Visit(SubtractionNode node);
    public abstract T Visit(MultiplicationNode node);
    public abstract T Visit(DivisionNode node);
    public abstract T Visit(NegateNode node);
    public abstract T Visit(NumberNode node);
    public abstract T Visit(AssignmentNode node);
    public abstract T Visit(BlockNode node);
    public abstract T Visit(IdNode node);
    public abstract T Visit(IdAtomNode node);
    public abstract T Visit(PrintNode node);
    public abstract T Visit(AndNode node);
    public abstract T Visit(OrNode node);
    public abstract T Visit(IfStatNode node);
    public abstract T Visit(ConditionBlockNode node);    
    public abstract T Visit(GTEQNode node);      
    public abstract T Visit(LTEQNode node);   
    public abstract T Visit(GTNode node);   
    public abstract T Visit(LTNode node);   
    public abstract T Visit(EQNode node);   
    public abstract T Visit(NEQNode node);
    public abstract T Visit(DeclarationNode node); 
    public abstract T Visit(WhileNode node); 
    public abstract T Visit(ArrayExprNode node); 
    public abstract T Visit(CallFunctExprNode node);     
    public abstract T Visit(FunctionDeclNode node);
    public abstract T Visit(FunctionStatNode node);
    public abstract T Visit(ParentExprNode node);
    public abstract T Visit(ReturnStatNode node);
    
    
    public T Visit(AbstractASTNode node)
    {
    	if (node instanceof AdditionNode) {
    	    return Visit((AdditionNode)node);
    	} else if (node instanceof SubtractionNode) {
    	    return Visit((SubtractionNode)node);
    	} else if (node instanceof MultiplicationNode) {
    	    return Visit((MultiplicationNode)node);
    	} else if (node instanceof DivisionNode) {
    	    return Visit((DivisionNode)node);
    	} else if (node instanceof NegateNode) {
    	    return Visit((NegateNode)node);
    	} else if (node instanceof NumberNode) {
    	    return Visit((NumberNode)node);
    	} else if (node instanceof AssignmentNode) {
    	    return Visit((AssignmentNode)node);
    	} else if (node instanceof BlockNode) {
    	    return Visit((BlockNode)node);
    	} else if (node instanceof PrintNode) {
    	    return Visit((PrintNode)node);
    	} else if (node instanceof IdAtomNode) {
			return Visit((IdAtomNode)node);
		}else if (node instanceof IdNode) {
			return Visit((IdNode)node);
		}else if (node instanceof AndNode) {
			return Visit((AndNode)node);
		}else if (node instanceof OrNode) {
			return Visit((OrNode)node);
		}else if (node instanceof IfStatNode) {
			return Visit((IfStatNode)node);
		}else if (node instanceof ConditionBlockNode) {
			return Visit((ConditionBlockNode)node);
		}else if (node instanceof GTEQNode) {
			return Visit((GTEQNode)node);
		}else if (node instanceof LTEQNode) {
			return Visit((LTEQNode)node);
		}else if (node instanceof GTNode) {
			return Visit((GTNode)node);
		}else if (node instanceof LTNode) {
			return Visit((LTNode)node);
		}else if (node instanceof EQNode) {
			return Visit((EQNode)node);
		}else if (node instanceof NEQNode) {
			return Visit((NEQNode)node);
		}else if (node instanceof DeclarationNode) {
			return Visit((DeclarationNode)node);
		}else if (node instanceof WhileNode) {
			return Visit((WhileNode)node);
		}else if (node instanceof ArrayExprNode) {
			return Visit((ArrayExprNode)node);
		}else if (node instanceof CallFunctExprNode) {
			return Visit((CallFunctExprNode)node);
		}else if (node instanceof FunctionDeclNode) {
			return Visit((FunctionDeclNode)node);
		}else if (node instanceof FunctionStatNode) {
			return Visit((FunctionStatNode)node);
		}else if (node instanceof ParentExprNode) {
			return Visit((ParentExprNode)node);
		}else if (node instanceof ReturnStatNode) {
			return Visit((ReturnStatNode)node);
		}
    	
		return null; 
    }
 

}