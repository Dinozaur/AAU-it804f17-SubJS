package jss;

import nodes.AbstractASTNode;
import nodes.AdditionNode;
import nodes.AndNode;
import nodes.ArrayExprNode;
import nodes.AssignmentNode;
import nodes.BlockNode;
import nodes.CallFunctExprNode;
import nodes.ConditionBlockNode;
import nodes.DeclarationNode;
import nodes.DivisionNode;
import nodes.EQNode;
import nodes.FunctionDeclNode;
import nodes.FunctionStatNode;
import nodes.GTEQNode;
import nodes.GTNode;
import nodes.IdAtomNode;
import nodes.IdNode;
import nodes.IfStatNode;
import nodes.LTEQNode;
import nodes.LTNode;
import nodes.MultiplicationNode;
import nodes.NEQNode;
import nodes.NegateNode;
import nodes.NumberNode;
import nodes.OrNode;
import nodes.ParentExprNode;
import nodes.PrintNode;
import nodes.ReturnStatNode;
import nodes.SubtractionNode;
import nodes.WhileNode;

public class temmp extends AstVisitor<Value>{
	 public static final double SMALL_VALUE = 0.00000000001;
		private static final SymtabInterface sti = new TeamSymtab();
		private static boolean DEBUG_MODE = false;
		
		@Override
		public Value Visit(AdditionNode node) {
			Value left = Visit(node.getLeft());
			Value right = Visit(node.getRight());
			Value val = null;
			 if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_INT){
				 val = new Value("new _Int(" + left.asString() + ").add(" + right.asString() + ")");
				 val.type = SubJSLexer.TYPE_INT;
			 } else if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value("new _Float(" + left.asString() + ").add(" + right.asString() + ")");
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value("new _Float(" + left.asString() + ").add(" + right.asString() + ")");
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value("new _Float(" + left.asString() + ").add(" + right.asString() + ")");
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else {
				 val = new Value("new _String(" + left.asString() + ").add(" + right.asString() + ")");
				 val.type = SubJSLexer.TYPE_STRING;
		        }
			
			
			//String result = left + " + " + right;
			return val;
		}

		@Override
		public Value Visit(SubtractionNode node) {
			Value left = Visit(node.getLeft());
			Value right = Visit(node.getRight());
			Value val = null;
			 if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_INT){
				 val = new Value("new _Int(" + left.asString() + ").subtract(" + right.asString() + ")");
				 val.type = SubJSLexer.TYPE_INT;
			 } else if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value("new _Float(" + left.asString() + ").subtract(" + right.asString() + ")");
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value("new _Float(" + left.asString() + ").subtract(" + right.asString() + ")");
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value("new _Float(" + left.asString() + ").subtract(" + right.asString() + ")");
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else {
		           	throw new RuntimeException("\nType mismatch in  - operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
		        }
			
			//String result = left + " - " + right;
			return val;
		}

		@Override
		public Value Visit(MultiplicationNode node) {
			Value left = Visit(node.getLeft());
			Value right = Visit(node.getRight());
			
			 Value val = null;
			 //Not sure about the semantics modify after
			 if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_INT){
				 val = new Value("new _Int(" + left.asString() + ").multiply(" + right.asString() + ")");
				 val.type = SubJSLexer.TYPE_INT;
			 } else if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value("new _Float(" + left.asString() + ").multiply(" + right.asString() + ")");
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value("new _Float(" + left.asString() + ").multiply(" + right.asString() + ")");
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value("new _Float(" + left.asString() + ").multiply(" + right.asString() + ")");
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else {
		           	throw new RuntimeException("\nType mismatch in  * operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
		        }
			
			//Value result = left + " * " + right;
			return val;
		}

		@Override
		public Value Visit(DivisionNode node) {
			Value left = Visit(node.getLeft());
			Value right = Visit(node.getRight());
			  Value val = null;
				 //Not sure about the semantics modify after
				 if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
					 val = new Value("new _Int(" + left.asString() + ").devide(" + right.asString() + ")");
					 val.type = SubJSLexer.TYPE_INT;
				 } else if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_FLOAT){
					 val = new Value("new _Float(" + left.asString() + ").devide(" + right.asString() + ")");
					 val.type = SubJSLexer.TYPE_FLOAT;
				 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
					 val = new Value("new _Float(" + left.asString() + ").devide(" + right.asString() + ")");
					 val.type = SubJSLexer.TYPE_FLOAT;
				 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
					 val = new Value("new _Float(" + left.asString() + ").devide(" + right.asString() + ")");
					 val.type = SubJSLexer.TYPE_FLOAT;
				 } else {
			           	throw new RuntimeException("\nType mismatch in  / operation. Left operant " 
			        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
			        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
			        }
			
			//Value result = left + " / " + right;
			return val;
		}

		@Override
		public Value Visit(NegateNode node) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Value Visit(NumberNode node) {
			Value val = null;
			switch (node.getValue().type) {
				case SubJSLexer.TYPE_INT:
					val = new Value(Integer.toString(node.getValue().asInteger()));
					 val.type = SubJSLexer.TYPE_INT;
					break;
				case SubJSLexer.TYPE_FLOAT:
					val = new Value(Double.toString(node.getValue().asDouble()));
					 val.type = SubJSLexer.TYPE_FLOAT;
					break;
				case SubJSLexer.TYPE_STRING:
					val = new Value('"' + node.getValue().asString() + '"');
					 val.type = SubJSLexer.TYPE_STRING;
					break;
				case SubJSLexer.TYPE_BOOLEAN:
					val = new Value(node.getValue().asBoolean().toString());
					 val.type = SubJSLexer.TYPE_BOOLEAN;
					break;
				default:
					return null;
		        }
			return val;
		}

		@Override
		public Value Visit(AssignmentNode node) {
			
			/*
			Value id = Visit(node.getIdNode());
			Value value = Visit(node.getExprNode());
			String str = id.asString();
			Value val = null;
			if(node.getArrayIdIndExprNode() != null){
				Value arrInd = Visit(node.getArrayIdIndExprNode());
				str = str + "[" + arrInd.asString() + "]";
			}
			str = str + " = " + value.asString() + ";";
			val = new Value(str);
			*/
			Value id = Visit(node.getIdNode());
			Value rhs = Visit(node.getExprNode());
			String str = id.asString();
			Value val = null;
			if(sti.contObject(id.asString())){
				Value lhs = sti.lookup(id.asString());
				 //Not sure about the semantics modify after
				if(lhs.type == rhs.type){
					if(node.getArrayIdIndExprNode() != null){
						Value valArrIndexExpress = Visit(node.getArrayIdIndExprNode());	
						Object[] al = (Object[])lhs.value;
						al[valArrIndexExpress.asInteger()] =  rhs.value ;
						Value vva = new Value(al);
						vva.type = lhs.type;
						vva.setArray(true);
						sti.enter(id.asString(), vva);
						str = str + "[" + valArrIndexExpress.asString() + "]";
					} else {
						sti.enter(id.asString(), rhs);
					}
				}else{
					//add more info about the error
		        	throw new RuntimeException("\n\nFaild to assign value of type " + SubJSLexer.ruleNames[rhs.type - 1]
		        			+ " to variable " + id.asString()
		        			+ " which has type " + SubJSLexer.ruleNames[sti.lookup(id.asString()).type - 1]);
				}
			}else{
	        	throw new RuntimeException("\nVariable '"  + id.asString() +  "' not declared.");
			}
			
			str = str + " = " + rhs.asString() + ";";
			val = new Value(str);
			return val;
		}

		@Override
		public Value Visit(BlockNode node) {
			String block = "";
			sti.incrNestLevel();
			for(AbstractASTNode en : node.getStat()){
				
				
				block +=Visit(en) + "\n";
	    	}
			Value val = new Value(block);
			sti.decrNestLevel();
			return val;
		}

		@Override
		public Value Visit(IdNode node) {
			Value val = null;
			val = new Value(node.getValue().asString());
			val.type = node.getValue().type;
			return val;
		}

		@Override
		public Value Visit(IdAtomNode node) {
			Value val = null;
			 if(node.getValue().type == SubJSLexer.TYPE_INT){
				 val = new Value(Integer.toString(node.getValue().asInteger()));
				 val.type = node.getValue().type;
			 } else if(node.getValue().type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(Double.toString(node.getValue().asDouble()));
				 val.type = node.getValue().type;
			 } else if(node.getValue().type == SubJSLexer.TYPE_BOOLEAN){
				 val = new Value(node.getValue().asBoolean().toString());
				 val.type = node.getValue().type;
			 }else if(node.getValue().type == SubJSLexer.TYPE_CHAR){
				 val = new Value(node.getValue().asString());
				 val.type = node.getValue().type;
			 }else if(node.getValue().type == SubJSLexer.TYPE_STRING){
				 val = new Value(node.getValue().asString());
				 val.type = node.getValue().type;
			 }else {
				 val = new Value(node.getValue().asString());
				 val.type = node.getValue().type;
			 }
				return val;
		}

		@Override
		public Value Visit(PrintNode node) {
			Value value = Visit(node.getInnerNode());
			Value prn = new Value("console.log(" +  value  + ");");
			return prn;
		}

		@Override
		public Value Visit(AndNode node) {
			Value left = Visit(node.getLeft());
			Value right = Visit(node.getRight());
			Value val = null;
			 if(left.type == right.type ){
		            if(left.type == SubJSLexer.TYPE_BOOLEAN ){
		            	val = new Value(left.toString() + " && " + right.toString());
		            }else{
		            	throw new RuntimeException("\nAnd operation defined only for BOOLEAN");
		            }
		            val.type = SubJSLexer.TYPE_BOOLEAN;
		        }else{
		           	throw new RuntimeException("\nType mismatch in And operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type - 1]);
		        }
			
			//String result = left + " && " + right;
			return val;
		}

		@Override
		public Value Visit(OrNode node) {
			Value left = Visit(node.getLeft());
			Value right = Visit(node.getRight());
			Value val = null;
			 if(left.type == right.type ){
		            if(left.type == SubJSLexer.TYPE_BOOLEAN ){
		            	val = new Value(left.toString() + " || " + right.toString());
		            }else{
		            	throw new RuntimeException("\nAnd operation defined only for BOOLEAN");
		            }
		            val.type = SubJSLexer.TYPE_BOOLEAN;
		        }else{
		           	throw new RuntimeException("\nType mismatch in And operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type - 1]);
		        }
			
			//String result = left + " || " + right;
			return val;
		}
		
		@Override
		public Value Visit(IfStatNode node) {
			String result = "if" + Visit(node.getConditionBlockNode().get(0));
			for(int i = 1; i < node.getConditionBlockNode().size(); i++){
				result += "else if" + Visit(node.getConditionBlockNode().get(i));
			}
			if(node.getBlockNode() != null){
				result +=  "else{\n" + Visit(node.getBlockNode()) + "}";
			}
			Value val = new Value(result);
			val.type = SubJSLexer.TYPE_STRING;
			return val;
		}

		@Override
		public Value Visit(ConditionBlockNode node) {
			Value expression = Visit(node.getExpNode());
			Value block = Visit(node.getBlockNode());
			String elcl = "(" + expression + ")" + "{\n" + block + "}";
			Value val = new Value(elcl);
			return val;
		}

		@Override
		public Value Visit(GTEQNode node) {
			Value left = Visit(node.getLeft());
			Value right = Visit(node.getRight());
			
			 Value val = null;
			 //Not sure about the semantics modify after
			 if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(Integer.toString(left.asInteger()) + " >= " + Integer.toString(right.asInteger()));
				 val.type = SubJSLexer.TYPE_INT;
			 } else if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(Integer.toString(left.asInteger()) + " >= " + Double.toString(right.asDouble()));
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(Double.toString(left.asDouble()) + ">=" + Integer.toString(right.asInteger()));
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(Double.toString(left.asDouble()) + ">=" + Double.toString(right.asDouble()));
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else{
		           	throw new RuntimeException("\nType mismatch in  >= operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
		     }
				
			//String result = left + " >= " + right;
			return val;
		}

		@Override
		public Value Visit(LTEQNode node) {
			Value left = Visit(node.getLeft());
			Value right = Visit(node.getRight());
			
			 Value val = null;
			 //Not sure about the semantics modify after
			 if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(Integer.toString(left.asInteger()) + " <= " + Integer.toString(right.asInteger()));
				 val.type = SubJSLexer.TYPE_INT;
			 } else if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(Integer.toString(left.asInteger()) + " <= " + Double.toString(right.asDouble()));
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(Double.toString(left.asDouble()) + " <= " + Integer.toString(right.asInteger()));
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(Double.toString(left.asDouble()) + " <= " + Double.toString(right.asDouble()));
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else{
		           	throw new RuntimeException("\nType mismatch in  <= operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
		     }
			
			//String result = left + " <= " + right;
			return val;
		}

		@Override
		public Value Visit(GTNode node) {
			Value left = Visit(node.getLeft());
			Value right = Visit(node.getRight());
			
			 Value val = null;
			 //Not sure about the semantics modify after
			 if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(Integer.toString(left.asInteger()) + " > " + Integer.toString(right.asInteger()));
				 val.type = SubJSLexer.TYPE_INT;
			 } else if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(Integer.toString(left.asInteger()) + " > " + Double.toString(right.asDouble()));
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(Double.toString(left.asDouble()) + " > " + Integer.toString(right.asInteger()));
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(Double.toString(left.asDouble()) + " > " + Double.toString(right.asDouble()));
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else{
		           	throw new RuntimeException("\nType mismatch in  > operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
		     }
			
			//String result = left + " > " + right;
			return val;
		}

		@Override
		public Value Visit(LTNode node) {
			Value left = Visit(node.getLeft());
			Value right = Visit(node.getRight());
			
			 Value val = null;
			 //Not sure about the semantics modify after
			 if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(Integer.toString(left.asInteger()) + " < " + Integer.toString(right.asInteger()));
				 val.type = SubJSLexer.TYPE_INT;
			 } else if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(Integer.toString(left.asInteger()) + " < " + Double.toString(right.asDouble()));
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(Double.toString(left.asDouble()) + " < " + Integer.toString(right.asInteger()));
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(Double.toString(left.asDouble()) + " < " + Double.toString(right.asDouble()));
				 val.type = SubJSLexer.TYPE_FLOAT;
			 } else{
		           	throw new RuntimeException("\nType mismatch in  < operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
		     }
			
			//String result = left + " < " + right;
			return val;
		}

		@Override
		public Value Visit(EQNode node) {
			Value left = Visit(node.getLeft());
			Value right = Visit(node.getRight());
			
			Value val = null;
			
			 if(left.isDouble() && right.isDouble()){
		        	val = new Value(Double.toString(left.asDouble()) + " == " + right.asDouble());
		        	val.type = SubJSLexer.TYPE_BOOLEAN;
		        } else if(left.isBoolean() && right.isBoolean()) {
		        	val = new Value(left.asBoolean().toString() + " == " + right.asBoolean().toString() );
		        	val = new Value(left.asBoolean().equals(right.asBoolean()));
		        	val.type = SubJSLexer.TYPE_BOOLEAN;
		        } else if(left.isString() && right.isString()) {
		        	val = new Value(left.asString() + " == " + right.asString());
		        	val.type = SubJSLexer.TYPE_BOOLEAN;
		        } else {
		        	throw new RuntimeException("\nType mismatch in EQ(=) operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type - 1]);
		        }
	     	return  val;
		}

		@Override
		public Value Visit(NEQNode node) {
			Value left = Visit(node.getLeft());
			Value right = Visit(node.getRight());
			
			Value val = null;
			
			 if(left.isDouble() && right.isDouble()){
		        	val = new Value(Double.toString(left.asDouble()) + " != " + right.asDouble());
		        	val.type = SubJSLexer.TYPE_BOOLEAN;
		        } else if(left.isBoolean() && right.isBoolean()) {
		        	val = new Value(left.asBoolean().toString() + " != " + right.asBoolean().toString() );
		        	val = new Value(left.asBoolean().equals(right.asBoolean()));
		        	val.type = SubJSLexer.TYPE_BOOLEAN;
		        } else if(left.isString() && right.isString()) {
		        	val = new Value(left.asString() + " != " + right.asString());
		        	val.type = SubJSLexer.TYPE_BOOLEAN;
		        } else {
		        	throw new RuntimeException("\nType mismatch in EQ(=) operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type - 1]);
		        }
	    	return  val;
		}

		@Override
		public Value Visit(DeclarationNode node) {
			Value variable = Visit(node.getIdNode());
			Value expr = Visit(node.getExprNode());
			String decl = "var " + variable; 
			if(node.getArraySizeExpression() != null){
				decl = decl + " = []"; 
			}
			decl = decl + " = "  + expr.asString() +  " ;";
			Value val = new Value(decl);
			val.type = expr.type;
			return val;
		}

		@Override
		public Value Visit(WhileNode node) {
			Value blockNode = Visit(node.getBlockNode());
			Value expr = Visit(node.getExpressionNode());
			String result = "while(" + expr.asString() + ")" + "{\n" + blockNode.asString() +  "}";
			
			Value val = new Value(result);
			
			return val;
		}

		@Override
		public Value Visit(ArrayExprNode node) {
			Value left = Visit(node.getLeft());
			Value right = Visit(node.getRight());
			String result = left.asString() + "[" + right.asString() + "]";
			Value val = new Value(result);
			return val;
		}

		@Override
		public Value Visit(FunctionDeclNode node) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Value Visit(CallFunctExprNode node) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Value Visit(FunctionStatNode node) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Value Visit(ParentExprNode node) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Value Visit(ReturnStatNode node) {
			// TODO Auto-generated method stub
			return null;
		}

}
