package jss;

public interface SymtabInterface {
   public void incrNestLevel();
   public void decrNestLevel();
   public int getCurrentNestLevel();
   public boolean contObject(String key);
   public String scopeNumber(String key);
   public Value lookup(String id);
   public void enter(String id, Value s);
   public void out(String message);
   public void err(String message);
}

