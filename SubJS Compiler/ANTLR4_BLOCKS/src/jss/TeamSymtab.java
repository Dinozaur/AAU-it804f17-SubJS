package jss;

import java.util.ArrayList;
import java.util.HashMap;

/** Solution authored by:
 * 
 *  ( team members' names )
 *
 */

public class TeamSymtab extends Symtab implements SymtabInterface {
	
	public ArrayList<HashMap<String, Value>> symTable = new ArrayList<HashMap<String, Value>>();
	/** Should never return a negative integer 
	 */
	
	public int getCurrentNestLevel() {
		return symTable.size();  // you must fix this
	}
	
	/** Opens a new scope, retaining outer ones */
	
	public void incrNestLevel() {
		symTable.add(new HashMap<String, Value>());
	}
	
	/** Closes the innermost scope */
	
	public void decrNestLevel() {
		if(symTable.size() > 0){
			symTable.remove(symTable.size() - 1);
		}
		
	}
	
	/** Enter the given symbol information into the symbol table.  If the given
	 *    symbol is already present at the current nest level, do whatever is most
	 *    efficient, but do NOT throw any exceptions from this method.
	 */
	
	public void enter(String s, Value info) {
		int scopeNumber = -1;
		if(symTable.size() > 0){
			for(int i = symTable.size() - 1; i >= 0; i--){
				if(symTable.get(i).get(s) != null){
					scopeNumber = i;
					symTable.get(i).put(s, info);
				}
			}
			
			if(scopeNumber == -1){	
				symTable.get(symTable.size() -1 ).put(s, info);
			}
		}
	}
	
	/** Returns the information associated with the innermost currently valid
	 *     declaration of the given symbol.  If there is no such valid declaration,
	 *     return null.  Do NOT throw any excpetions from this method.
	 */
	
	public Value lookup(String s) {
		int scopeNumber = -1;
		if(symTable.size() > 0){
			for(int i = symTable.size() - 1; i >= 0; i--){
				if(symTable.get(i).get(s) != null){
					scopeNumber = i;
					return symTable.get(i).get(s);
				}
			}
		}
		else{
			return null;
		}
		return null;
	}
	
	public boolean contObject(String key) {
		if(symTable.size() > 0){
			int scopeNumber = -1;
			for(int i = symTable.size() - 1; i >= 0; i--){
				if(symTable.get(i).containsKey(key)){
					scopeNumber = i;
					return true;
				}
			}
		}else{
			return false;
		}
		return false;
	}
	
	
	public String scopeNumber(String key) {
		if(symTable.size() > 0){
			for(int i = symTable.size() - 1; i >= 0; i--){
				if(symTable.get(i).containsKey(key)){
					return Integer.toString(i);
				}
			}
		}else{
			return "-1";
		}
		return "-1";
	}


	
}
