package jss;

import jss.SubJSParser.SubJSContext;
import nodes.*;

import org.antlr.v4.gui.Trees;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;


    public class Main 
    {
        public static void main( String[] args) throws Exception 
        {
            //Can also have hardcode input
      	   //String inp = "12*(5-6)-3*(1-2)\n";
            //CharStream stream = new ANTLRInputStream(inp);
        	String fileName = "a.mu";
        	//String fileName = "test12.mu";
        	//String fileName = "test13.mu";
        	//String fileName = "feature_number_n.mu";
        	//String fileName = "scope.mu";
        	//String fileName = "test.mu";
        	//String fileName = "test_nes_lev.mu";
        	//String fileName = "test_func.mu";
            ANTLRInputStream stream = new ANTLRFileStream(fileName);
            SubJSLexer lexer = new SubJSLexer(stream);
            SubJSParser parser = new SubJSParser(new CommonTokenStream(lexer));
            SubJSContext cst = parser.subJS();
            
            //Trees.inspect(cst, parser);
            AbstractASTNode ast = new BuildAstVisitor().visitSubJS(cst);


            System.out.println("==================================================================");
            System.out.println("====================Static checking and value evaluation==========");
            System.out.println("==================================================================");
            StaticCheckingVisitor scv = new StaticCheckingVisitor();
            scv.setDEBUG_MODE(true);
            Value value1 = scv.Visit(ast);
            //System.out.println("Eval= " + value1);
            
            System.out.println("==================================================================");
            System.out.println("====================Code generation JavaScript====================");
            System.out.println("==================================================================");         
            GenerateCodeVisitor gcv = new GenerateCodeVisitor();
            gcv.setDEBUG_MODE(false);
            String value2 = gcv.Visit(ast).getGenCode();
            System.out.println(value2);
            
            
            /*
            Field fld[] = SubJSLexer.class.getDeclaredFields();
            for (int i = 0; i < fld.length; i++)
            {
                System.out.println("Variable Name is : " + fld[i].getName());
            } 
            */
            
         
        }
    }