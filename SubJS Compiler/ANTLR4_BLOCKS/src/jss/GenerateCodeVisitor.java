package jss;

import java.util.ArrayList;
import java.util.List;

import nodes.*;

public class GenerateCodeVisitor extends AstVisitor<Value>{

	 public static final double SMALL_VALUE = 0.00000000001;
		private static final SymtabInterface sti = new TeamSymtab();
		private static boolean DEBUG_MODE = false;
		private String lastFuncCall;
		@Override       
	    public Value Visit(BlockNode node) {
			sti.incrNestLevel();
			
			String block = "_scopeIndex++;\n";
			block += "_scope[_scopeIndex] = {};\n";
			Value val = null;
	    	for(AbstractASTNode en : node.getStat()){
	    		val = Visit(en); 
	    		//if(val != null){
	    		//	 break;
	    		//}
	    		block += val.getGenCode() + "\n";
	    	}
			block += "_scope[_scopeIndex] = {};\n";
			block += "_scopeIndex--;\n";
			val.setGenCode(block);
	    	sti.decrNestLevel();
	        return val;
	    }
		
		@Override
		 public Value Visit(AssignmentNode node){
			Value id = Visit(node.getIdNode());
			Value valArrIndexExpress = Visit(node.getArrayIdIndExprNode());	
			Value rhs = Visit(node.getExprNode());
			String rhsStr = rhs.getGenCode();
			
			String str = id.asString();
			str =  "_scope[_findScope(\"" + str + "\")]." + str;
		//	str =  sti.scopeNumber(str) +  str;
			
			if(sti.contObject(id.asString())){
				Value lhs = sti.lookup(id.asString());
				 //Not sure about the semantics modify after
				if(lhs.type == rhs.type){
					if(node.getArrayIdIndExprNode() != null){
						
						Value vva = new Value(null);
						switch (lhs.type) {
						case SubJSLexer.TYPE_INT:
							int[] arr1 = (int[])lhs.value;
							arr1[valArrIndexExpress.asInteger()] =  (int) rhs.value ;
							vva = new Value(arr1);
							vva.type = lhs.type;
							vva.setArray(true);
							sti.enter(id.asString(), vva);
							break;
						case SubJSLexer.TYPE_FLOAT:
							double[] arr2 = (double[])lhs.value;
							arr2[valArrIndexExpress.asInteger()] =  (double) rhs.value ;
							vva = new Value(arr2);
							vva.type = lhs.type;
							vva.setArray(true);
							sti.enter(id.asString(), vva);
							break;
						case SubJSLexer.TYPE_STRING:
							String[] arr3 = (String[])lhs.value;
							arr3[valArrIndexExpress.asInteger()] =  (String) rhs.value ;
							vva = new Value(arr3);
							vva.type = lhs.type;
							vva.setArray(true);
							sti.enter(id.asString(), vva);
							break;
						case SubJSLexer.TYPE_BOOLEAN:
							Boolean[] arr4 = (Boolean[])lhs.value;
							arr4[valArrIndexExpress.asInteger()] =  (Boolean) rhs.value ;
							vva = new Value(arr4);
							vva.type = lhs.type;
							vva.setArray(true);
							sti.enter(id.asString(), vva);
							break;
						default:
							return null;
				        }

						str = str + "[" + valArrIndexExpress.getGenCode() + "]";

					} else {
						sti.enter(id.asString(), rhs);
					}
					
					if(rhsStr.matches(".*.val[ ]*")){
						rhsStr = replaceLast(rhsStr, ".val", ""  );	
					}
					str = str + " = " + rhsStr + ";";
					
				}else{
					//add more info about the error
		        	throw new RuntimeException("\n\nFaild to assign value of type " + SubJSLexer.ruleNames[rhs.type - 1]
		        			+ " to variable " + id.asString()
		        			+ " which has type " + SubJSLexer.ruleNames[sti.lookup(id.asString()).type - 1]);
				}
			}else{
	        	throw new RuntimeException("\nVariable '"  + id.asString() +  "' not declared.");
			}
			
			Value val = new Value(null);
			val.setGenCode(str);
			return val;//value
		 }
		
		@Override
	    public Value Visit(AdditionNode node) {
	        Value left = Visit(node.getLeft());
	        Value right = Visit(node.getRight()); 
	        Value val = null;
	        	 if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_INT){
	    			 val = new Value(left.asInteger()  + right.asInteger());
	    			 val.setGenCode("new _Int(" + left.getGenCode() + " ).add(" + right.getGenCode() + ").val");
	    			 val.type = SubJSLexer.TYPE_INT;
	    		 } else if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_FLOAT){
	    			 val = new Value(left.asInteger()  +  right.asDouble());
	    			 val.setGenCode("new _Float(" + left.getGenCode() + " ).add(" + right.getGenCode() + " ).val");
	    			 val.type = SubJSLexer.TYPE_FLOAT;
	    		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
	    			 val = new Value(left.asDouble()  +  right.asInteger());
	    			 val.setGenCode("new _Float(" + left.getGenCode() + " ).add(" + right.getGenCode() + " ).val");
	    			 val.type = SubJSLexer.TYPE_FLOAT;
	    		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
	    			 val = new Value(left.asDouble()  +  right.asDouble());
	    			 val.setGenCode("new _Float(" + left.getGenCode() + " ).add(" + right.getGenCode() + " ).val");
	    			 val.type = SubJSLexer.TYPE_FLOAT;
	    		 } else {
	    			 val = new Value(left.asString() + right.asString());
	    			 val.setGenCode("new _String(" + left.getGenCode() + " ).concats(" + right.getGenCode() + " ).val");
	    			 val.type = SubJSLexer.TYPE_STRING;
	    }
	        
			return val;
	    }
		
		@Override
	    public Value Visit(SubtractionNode node){
	        Value left = Visit(node.getLeft());
	        Value right = Visit(node.getRight());
	        Value val = null;
			 //Not sure about the semantics modify after
	        if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_INT){
   			 val = new Value(left.asInteger()  - right.asInteger());
   			 val.setGenCode("new _Int(" + left.getGenCode() + " ).subtract(" + right.getGenCode() + " ).val");
   			 val.type = SubJSLexer.TYPE_INT;
   		 } else if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_FLOAT){
   			 val = new Value(left.asInteger()  -  right.asDouble());
   			 val.setGenCode("new _Float(" + left.getGenCode() + " ).subtract(" + right.getGenCode() + " ).val");
   			 val.type = SubJSLexer.TYPE_FLOAT;
   		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
   			 val = new Value(left.asDouble()  -  right.asInteger());
   			 val.setGenCode("new _Float(" + left.getGenCode() + " ).subtract(" + right.getGenCode() + " ).val");
   			 val.type = SubJSLexer.TYPE_FLOAT;
   		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
   			 val = new Value(left.asDouble() -  right.asDouble());
   			 val.setGenCode("new _Float(" + left.getGenCode() + " ).subtract(" + right.getGenCode() + " ).val");
   			 val.type = SubJSLexer.TYPE_FLOAT;
   		 } else {
   			 //throw
   			 val.type = SubJSLexer.TYPE_STRING;
   }

			return val;
	    }
		
		@Override
	    public Value Visit(MultiplicationNode node){
	        Value left = Visit(node.getLeft());
	        Value right = Visit(node.getRight());
	        Value val = null;
			 //Not sure about the semantics modify after
	        if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_INT){
	   			 val = new Value(left.asInteger()  * right.asInteger());
	   			 val.setGenCode("new _Int(" + left.getGenCode() + " ).multiply(" + right.getGenCode() + " ).val");
	   			 val.type = SubJSLexer.TYPE_INT;
	   		 } else if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_FLOAT){
	   			 val = new Value(left.asInteger() *  right.asDouble());
	   			 val.setGenCode("new _Float(" + left.getGenCode() + " ).multiply(" + right.getGenCode() + " ).val");
	   			 val.type = SubJSLexer.TYPE_FLOAT;
	   		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
	   			 val = new Value(left.asDouble()  *  right.asInteger());
	   			 val.setGenCode("new _Float(" + left.getGenCode() + " ).multiply(" + right.getGenCode() + " ).val");
	   			 val.type = SubJSLexer.TYPE_FLOAT;
	   		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
	   			 val = new Value(left.asDouble() *  right.asDouble());
	   			 val.setGenCode("new _Float(" + left.getGenCode() + " ).multiply(" + right.getGenCode() + " ).val");
	   			 val.type = SubJSLexer.TYPE_FLOAT;
	   		 } else {
		 //throw
 			 val.type = SubJSLexer.TYPE_STRING;	
		   throw new RuntimeException("\nType mismatch in  * operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
		        }

			return val;
	    }
		
		@Override
	    public Value Visit(DivisionNode node) {
	        Value left = Visit(node.getLeft());
	        Value right = Visit(node.getRight());

	        Value val = null;
			 //Not sure about the semantics modify after
	        if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_INT){
	   			 val = new Value(left.asInteger()  / right.asInteger());
	   			 val.setGenCode("new _Int(" + left.getGenCode() + " ).devide(" + right.getGenCode() + " ).val");
	   			 val.type = SubJSLexer.TYPE_INT;
	   		 } else if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_FLOAT){
	   			 val = new Value(left.asInteger() /  right.asDouble());
	   			 val.setGenCode("new _Float(" + left.getGenCode() + " ).devide(" + right.getGenCode() + " ).val");
	   			 val.type = SubJSLexer.TYPE_FLOAT;
	   		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
	   			 val = new Value(left.asDouble()  /  right.asInteger());
	   			 val.setGenCode("new _Float(" + left.getGenCode() + " ).devide(" + right.getGenCode() + " ).val");
	   			 val.type = SubJSLexer.TYPE_FLOAT;
	   		 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
	   			 val = new Value(left.asDouble() /  right.asDouble());
	   			 val.setGenCode("new _Float(" + left.getGenCode() + " ).devide(" + right.getGenCode() + " ).val");
	   			 val.type = SubJSLexer.TYPE_FLOAT;
	   		 } else {
		 //throw
 			 val.type = SubJSLexer.TYPE_STRING;	
		   throw new RuntimeException("\nType mismatch in / operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
		        }

			return val;
	    }
		
		@Override
	    public Value Visit(AndNode node) {
	        Value left = Visit(node.getLeft());
	        Value right = Visit(node.getRight());
	        Value result = null;
			 //Not sure about the semantics modify after
	        if(left.type == right.type ){
	            if(left.type == SubJSLexer.TYPE_BOOLEAN ){
	            	result = new Value(left.asBoolean() && right.asBoolean());
	            	result.setGenCode( left.getGenCode() + " && " + right.getGenCode() );
	            }else{
	            	throw new RuntimeException("\nAnd operation defined only for BOOLEAN");
	            }
	        	result.type = left.type;
	        }else{
	           	throw new RuntimeException("\nType mismatch in And operation. Left operant " 
	        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
	        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type - 1]);
	        }
			return result;
	    }
		
		@Override
	    public Value Visit(OrNode node) {
	        Value left = Visit(node.getLeft());
	        Value right = Visit(node.getRight());
	        Value result = null;
			 //Not sure about the semantics modify after
	        if(left.type == right.type ){
	            if(left.type == SubJSLexer.TYPE_BOOLEAN ){
	            	result = new  Value(left.asBoolean() || right.asBoolean());
	            	result.setGenCode( left.getGenCode() + " || " + right.getGenCode() );
	            }else{
	            	throw new RuntimeException("\nOr operation defined only for BOOLEAN");
	            }
	        	result.type = left.type;
	        }else{
	           	throw new RuntimeException("\nType mismatch in Or operation. Left operant " 
	        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
	        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type - 1]);
	        }
			return result;
	    }

		@Override
	    public  Value Visit(NegateNode node){
			 Value value = Visit(node.getInnerNode());
		     Value result = null;
			 //Not sure about the semantics modify after
		     if(value.type == SubJSLexer.TYPE_INT || value.type == SubJSLexer.TYPE_FLOAT){
		     	result = new Value(-value.asDouble());
		     	result.setGenCode( "-" + value.getGenCode());
		     }else{
		     	throw new RuntimeException("\nMultiplication operation defined only for INT and FLOAT");
		     }
		     result.type = value.type;
			return result;
	    }
		
		@Override
	    public Value Visit(NumberNode node) {
			Value val = node.getValue();
			switch (val.type) {
				case SubJSLexer.TYPE_INT:
					val.setGenCode("new _Int(" + Integer.toString(node.getValue().asInteger()) + ").val ");
					 val.type = SubJSLexer.TYPE_INT;
					break;
				case SubJSLexer.TYPE_FLOAT:
					val.setGenCode("new _Float(" + Double.toString(node.getValue().asDouble()) + ").val");
					 val.type = SubJSLexer.TYPE_FLOAT;
					break;
				case SubJSLexer.TYPE_STRING:
					val.setGenCode("new _String(" + '"' + node.getValue().asString() + '"' + ").val");
					 val.type = SubJSLexer.TYPE_STRING;
					break;
				case SubJSLexer.TYPE_BOOLEAN:
					val.setGenCode("new _Bool(" + node.getValue().asBoolean().toString() + ").val");
					 val.type = SubJSLexer.TYPE_BOOLEAN;
					break;
				default:
					return null;
		        }
			return val;
	    }
		
		
		@Override
	    public Value Visit(IdNode node) {
	        return node.getValue();
	    }

		@Override
		public Value Visit(PrintNode node) {
			Value value = Visit(node.getInnerNode());
		     Value result = new Value(null);
			if(isDEBUG_MODE()){
				System.out.println(value);
			}
			result.setGenCode("console.log(" +  value.getGenCode()  + ");");
			return result;
		}
		
		@Override
	    public Value Visit(IdAtomNode node) {
			String idstr = node.getValue().asString();
			
			Value val = sti.lookup(idstr);
			
			idstr =  "_scope[_findScope(\"" + idstr + "\")]." + idstr;
			
			//String str = id.asString();
			//idstr = sti.scopeNumber(idstr) + idstr;
			
			
			
			switch (val.type) {
			case SubJSLexer.TYPE_INT:
				if(val.isArray()){
					val.setGenCode( idstr);
				}else{
					val.setGenCode("new _Int(" + idstr + " .val).val");
				}
				val.type = SubJSLexer.TYPE_INT;
				break;
			case SubJSLexer.TYPE_FLOAT:
				if(val.isArray()){
					val.setGenCode( idstr);
				}else{
					val.setGenCode("new _Float(" + idstr + ".val).val");
				}
				 val.type = SubJSLexer.TYPE_FLOAT;
				break;
			case SubJSLexer.TYPE_STRING:
				if(val.isArray()){
					val.setGenCode( idstr);
				}else{
					val.setGenCode("new _String(" + idstr + " .val).val");
				}
				 val.type = SubJSLexer.TYPE_STRING;
				break;
			case SubJSLexer.TYPE_BOOLEAN:
				if(val.isArray()){
					val.setGenCode( idstr);
				}else{
					val.setGenCode("new _Bool(" + idstr + ".val).val");
				}
				 val.type = SubJSLexer.TYPE_BOOLEAN;
				break;
			default:
				return null;
	        }
			
	        return val;
	    }
		
		@Override
		public Value Visit(IfStatNode node) {
			String result = "if" + Visit(node.getConditionBlockNode().get(0)).getGenCode();
			for(int i = 1; i < node.getConditionBlockNode().size(); i++){
				result += "else if" + Visit(node.getConditionBlockNode().get(i)).getGenCode();
			}
			if(node.getBlockNode() != null){
				result +=  "else{\n" + Visit(node.getBlockNode()).getGenCode() + "}";
			}
			Value val = new Value(null);
			val.setGenCode(result);
			val.type = SubJSLexer.TYPE_STRING;
			return val;
		}
		
		@Override
		public Value Visit(WhileNode node) {
			Value blockNode = Visit(node.getBlockNode());
			Value expr = Visit(node.getExpressionNode());
			String result = "while(" + expr.getGenCode() + ")" + "{\n" + blockNode.getGenCode() +  "}";
			
			Value val = new Value(null);
			val.setGenCode(result);
			return val;
		}
		
		@Override
		public Value Visit(ConditionBlockNode node) {
			Value expression = Visit(node.getExpNode());
			Value block = Visit(node.getBlockNode());
			String elcl = "(" + expression.getGenCode() + ")" + "{\n" + block.getGenCode()  + "}";
			Value val = new Value(null);
			val.setGenCode(elcl);
			return val;
		}

		@Override
		public Value Visit(GTEQNode node) {
			 Value left = Visit(node.getLeft());
			 Value right = Visit(node.getRight());
			 Value val = null;
			 //Not sure about the semantics modify after
			 if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(left.asInteger()  >= right.asInteger());
				 val.setGenCode("new _Int(" + left.getGenCode() + ").greaterThanEqual(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(left.asInteger()  >=  right.asDouble());
				 val.setGenCode("new _Float(" + left.getGenCode() + ").greaterThanEqual(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(left.asDouble()  >= right.asInteger());
				 val.setGenCode("new _Float(" + left.getGenCode() + ").greaterThanEqual(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(left.asDouble()  >=  right.asDouble());
				 val.setGenCode("new _Float(" + left.getGenCode() + ").greaterThanEqual(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else {
		           	throw new RuntimeException("\nType mismatch in  >= operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
		        }
	      return val;
		}
		
		@Override
		public Value Visit(LTEQNode node) {
			 Value left = Visit(node.getLeft());
			 Value right = Visit(node.getRight());
			 Value val = null;
			 
			 if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(left.asInteger()  <=  right.asInteger());
				 val.setGenCode("new _Int(" + left.getGenCode() + ").lessThanEqual(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(left.asInteger()  <=  right.asDouble());
				 val.setGenCode("new _Float(" + left.getGenCode() + ").lessThanEqual(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(left.asDouble()  <=  right.asInteger());
				 val.setGenCode("new _Float(" + left.getGenCode() + ").lessThanEqual(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(left.asDouble()  <=  right.asDouble());
				 val.setGenCode("new _Float(" + left.getGenCode() + ").lessThanEqual(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else {
		           	throw new RuntimeException("\nType mismatch in  <= operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
		        }
	       return val;
		}
		
		@Override
		public Value Visit(GTNode node) {
			 Value left = Visit(node.getLeft());
			 Value right = Visit(node.getRight());
			 Value val = null;
			 
			 if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(left.asInteger()  >  right.asInteger());
				 val.setGenCode("new _Int(" + left.getGenCode() + ").greaterThan(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(left.asInteger()  >  right.asDouble());
				 val.setGenCode("new _Float(" + left.getGenCode() + ").greaterThan(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(left.asDouble()  >  right.asInteger());
				 val.setGenCode("new _Float(" + left.getGenCode() + ").greaterThan(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT && right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(left.asDouble()  >  right.asDouble());
				 val.setGenCode("new _Float(" + left.getGenCode() + ").greaterThan(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else {
		           	throw new RuntimeException("\nType mismatch in  > operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
		        }

	       return val;  
		}
		
		@Override
		public Value Visit(LTNode node) {
			 Value left = Visit(node.getLeft());
			 Value right = Visit(node.getRight());
	 
			 Value val = null;
			 
			 if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(left.asInteger()  <  right.asInteger());
				 val.setGenCode("new _Int(" + left.getGenCode() + ").lessThan(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(left.asInteger()  <  right.asDouble());
				 val.setGenCode("new _Float(" + left.getGenCode() + ").lessThan(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
				 val = new Value(left.asDouble()  <  right.asInteger());
				 val.setGenCode("new _Float(" + left.getGenCode() + ").lessThan(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 val = new Value(left.asDouble()  <  right.asDouble());
				 val.setGenCode("new _Float(" + left.getGenCode() + ").lessThan(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
			 } else {
		           	throw new RuntimeException("\nType mismatch in  < operation. Left operant " 
		        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
		        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type- 1]);
		        }

	        return val;
		}
		
		@Override
		public Value Visit(EQNode node) {
	        Value left = Visit(node.getLeft());
	        Value right = Visit(node.getRight());
			 //Not sure about the semantics modify after
	        if(left.type == SubJSLexer.TYPE_INT &&  right.type == SubJSLexer.TYPE_INT){
	        	Value val = new Value(Math.abs(left.asInteger() - right.asInteger()) < SMALL_VALUE);
	        	val.setGenCode("new _Int(" + left.getGenCode() + ") == new _Int(" + right.getGenCode() + ").val");
	        	val.type = SubJSLexer.TYPE_BOOLEAN;
	        	return  val;
			 } else if(left.type == SubJSLexer.TYPE_INT && right.type == SubJSLexer.TYPE_FLOAT){
				 Value val = new Value(Math.abs(left.asInteger() - right.asDouble()) < SMALL_VALUE);
		        	val.setGenCode("new _Int(" + left.getGenCode() + ") == new _Float(" + right.getGenCode() + ").val");	
				 val.type = SubJSLexer.TYPE_BOOLEAN;
		        	return  val;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_INT){
				 Value val = new Value(Math.abs(left.asDouble() - right.asInteger()) < SMALL_VALUE);
				 val.setGenCode("new _Float(" + left.getGenCode() + ") == new _Int(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
		        return  val;
			 } else if(left.type == SubJSLexer.TYPE_FLOAT &&  right.type == SubJSLexer.TYPE_FLOAT){
				 Value val = new Value(Math.abs(left.asDouble() - right.asDouble()) < SMALL_VALUE);
				 val.setGenCode("new _Float(" + left.getGenCode() + ") == new _Float(" + right.getGenCode() + ").val");
				 val.type = SubJSLexer.TYPE_BOOLEAN;
		         return  val;
			 } else if(left.type == SubJSLexer.TYPE_BOOLEAN && right.type == SubJSLexer.TYPE_BOOLEAN) {
	        	Value val = new Value(left.asBoolean().equals(right.asBoolean()));
	        	val.setGenCode("new _Bool(" + left.getGenCode() + ") == new _Bool(" + right.getGenCode() + ").val");
	        	val.type = SubJSLexer.TYPE_BOOLEAN;
	        	return val;
	        } else if(left.type == SubJSLexer.TYPE_STRING && right.type == SubJSLexer.TYPE_STRING ) {
	        	Value val = new Value(left.asString().equals(right.asString()));
	        	val.setGenCode("new _String(" + left.getGenCode() + ") == new _String(" + right.getGenCode() + ").val");
	        	val.type = SubJSLexer.TYPE_BOOLEAN;
	        	return val;
	        } else {
	        	throw new RuntimeException("\nType mismatch in EQ(=) operation. Left operant " 
	        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
	        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type - 1]);
	        }
		}
		
		@Override
		public Value Visit(NEQNode node) {
	        Value left = Visit(node.getLeft());
	        Value right = Visit(node.getRight()); 
			 //Not sure about the semantics modify after
	        if(left.isDouble() && right.isDouble()){
	        	Value val = new Value(Math.abs(left.asDouble() - right.asDouble())  >= SMALL_VALUE);
	        	val.type = SubJSLexer.TYPE_BOOLEAN;
	        	return  val; 	
	        } else if(left.isBoolean() && right.isBoolean()) {
	        	Value val = new Value(!left.asBoolean().equals(right.asBoolean()));
	        	val.type = SubJSLexer.TYPE_BOOLEAN;
	        	return val;
	        } else if(left.isString() && right.isString()) {
	        	Value val = new Value(!left.asString().equals(right.asString()));
	        	val.type = SubJSLexer.TYPE_BOOLEAN;
	        	return val;
	        } else {
	        	throw new RuntimeException("\nType mismatch in NEQ(!=) operation. Left operant " 
	        			+ left.asString() + " with type " + SubJSLexer.ruleNames[left.type - 1]  + " uncompatible with "
	        			+ " right operant " + right.asString() +  " of type " + SubJSLexer.ruleNames[right.type - 1]);
	        } 
		}
		 public static String replaceLast(String text, String regex, String replacement) {
		        return text.replaceFirst("(?s)(.*)" + regex, "$1" + replacement);
		    }

		@Override
		public Value Visit(DeclarationNode node) {
			//Need to check if variable is already defined
			node.getIdNode().getValue().type = node.getNodeType();
			
			Value variable = Visit(node.getIdNode());
			
			Value v = null;
			String decl = ""; 
			if(node.getArraySizeExpression() == null){
				if(node.getExprNode() != null){
					v = Visit(node.getExprNode());
				}
				else{
					v = new Value(null);
				}
				v.setArray(false);


				if(v.getGenCode().matches(".*.val[ ]*")){
					v.setGenCode(replaceLast(v.getGenCode(), ".val", ""  ));	
				}
			//	v.setGenCode(v.getGenCode().substring(0, v.getGenCode().replaceFirst("\\s++$", "").length() - 3) );

				
				
				decl = decl + " = "  + v.getGenCode() +  " ;";
			} else{

				Value arrSize = Visit(node.getArraySizeExpression());
				int size = arrSize.asInteger();
				String temps = "";
				switch (node.getNodeType()) {
				case SubJSLexer.TYPE_INT:
					int[] arr1 = new int[size];
					temps = " = [" + Visit(node.getExprNode()).getGenCode();
					if(temps.matches(".*.val[ ]*")){
						temps = replaceLast(temps, ".val", ""  );	
					}
					decl = decl + " " +  temps;
					for(int i = 1; i < size; i++){
						arr1[i] =   Visit(node.getExprNode()).asInteger();
						temps = Visit(node.getExprNode()).getGenCode();
						if(temps.matches(".*.val[ ]*")){
							temps = replaceLast(temps, ".val", ""  );	
						}
						decl = decl + " , " +  temps;
					}
					v = new Value(arr1);
					break;
				case SubJSLexer.TYPE_FLOAT:
					double[] arr2 = new double[size];
					temps = " = [" + Visit(node.getExprNode()).getGenCode();
					if(temps.matches(".*.val[ ]*")){
						temps = replaceLast(temps, ".val", ""  );	
					}
					decl = decl + " " +  temps;
					for(int i = 1; i < size; i++){
						arr2[i] =   Visit(node.getExprNode()).asDouble();
						temps = Visit(node.getExprNode()).getGenCode();
						if(temps.matches(".*.val[ ]*")){
							temps = replaceLast(temps, ".val", ""  );	
						}
						decl = decl + " , " +  temps;
					}
					break;
				case SubJSLexer.TYPE_STRING:
					String[] arr3 = new String[size];
					temps = " = [" + Visit(node.getExprNode()).getGenCode();
					if(temps.matches(".*.val[ ]*")){
						temps = replaceLast(temps, ".val", ""  );	
					}
					decl = decl + " " +  temps;
					for(int i = 1; i < size; i++){
						arr3[i] =   Visit(node.getExprNode()).asString();
						 temps = Visit(node.getExprNode()).getGenCode();
						if(temps.matches(".*.val[ ]*")){
							temps = replaceLast(temps, ".val", ""  );	
						}
						decl = decl + " , " +  temps;
					}
					break;
				case SubJSLexer.TYPE_BOOLEAN:
					Boolean[] arr4 = new Boolean[size];
					temps = " = [" + Visit(node.getExprNode()).getGenCode();
					if(temps.matches(".*.val[ ]*")){
						temps = replaceLast(temps, ".val", ""  );	
					}
					decl = decl + " " +  temps;
					for(int i = 1; i < size; i++){
						arr4[i] =   Visit(node.getExprNode()).asBoolean();
						temps = Visit(node.getExprNode()).getGenCode();
						if(temps.matches(".*.val[ ]*")){
							temps = replaceLast(temps, ".val", ""  );	
						}
						decl = decl + " , " +  temps;
					}
					break;
				default:
					return null;
		        }

				v.setArray(true);
				decl = decl + " ] ";
			}
			v.type = node.getNodeType();
			sti.enter(node.getIdNode().getValue().asString(), v);
			//Need to add the type possible new type

			//String lev = sti.scopeNumber(node.getIdNode().getValue().asString());
			String beg = "_scope[_scopeIndex]." + variable.asString();
			decl = beg + " " + decl; 
			Value ret = new Value(null);
			ret.setGenCode(decl);
			return ret;
		}

		@Override
		public Value Visit(ArrayExprNode node) {
			
			Value left = Visit(node.getLeft());
			Value right = Visit(node.getRight());
			Value tempArray = sti.lookup(left.asString());
			Object tempElement = null;
			Value val = null;
			int arrSize = 0;

			
			String pref = "";
			String sufix = " ).val";
			switch (tempArray.type) {
			case SubJSLexer.TYPE_INT:
				arrSize = ((int[])tempArray.value).length;
				pref = "new _Int(";
				break;
			case SubJSLexer.TYPE_FLOAT:
				arrSize = ((double[])tempArray.value).length;
				pref = "new _Float(";
				break;
			case SubJSLexer.TYPE_STRING:
				arrSize = ((String[])tempArray.value).length;
				pref = "new _String(";
				break;
			case SubJSLexer.TYPE_BOOLEAN:
				arrSize = ((Boolean[])tempArray.value).length;
				pref = "new _Bool(";
				break;
			default:
				return null;
	        }

			if(arrSize > right.asInteger()){
				switch (tempArray.type) {
				case SubJSLexer.TYPE_INT:
					
					tempElement = ((int[])tempArray.value )[right.asInteger()];
					break;
				case SubJSLexer.TYPE_FLOAT:
					tempElement = ((double[])tempArray.value )[right.asInteger()];
					break;
				case SubJSLexer.TYPE_STRING:
					tempElement = ((String[])tempArray.value )[right.asInteger()];
					break;
				case SubJSLexer.TYPE_BOOLEAN:
					tempElement = ((Boolean[])tempArray.value )[right.asInteger()];
					break;
				default:
					return null;
		        }
				val = new Value(tempElement);
			 val.type = tempArray.type;
				
				
			}else{
				int sds = tempArray.type;
				switch (sds) {
				case SubJSLexer.TYPE_INT:
					 tempElement = new Integer(123);
						val = new Value(tempElement);
					 val.type = SubJSLexer.TYPE_INT;
					break;
				case SubJSLexer.TYPE_FLOAT:
					 tempElement = new Double(12.3);
						val = new Value(tempElement);
					 val.type = SubJSLexer.TYPE_FLOAT;
					break;
				case SubJSLexer.TYPE_STRING:
					 tempElement = new String("asd");
						val = new Value(tempElement);
					 val.type = SubJSLexer.TYPE_STRING;
					break;
				case SubJSLexer.TYPE_BOOLEAN:
					 tempElement = new Boolean(true);
						val = new Value(tempElement);
					 val.type = SubJSLexer.TYPE_BOOLEAN;
					break;
				default:
					return null;
		        }
			}
			String str = left.asString();
			
			String lev = sti.scopeNumber(str);
			String beg = "_scope[_findScope(\"" + str + "\")]." + str;

			String result = pref +  beg + "[" + right.getGenCode() + "].val" + sufix;
			val.setArray(true);
			val.setGenCode(result);
			return val;

		}

		@Override
		public Value Visit(FunctionDeclNode node) {
			Value ind = Visit(node.getIdNode());
			Value result = new Value(node);
			result.type = node.getNodeType();
			sti.enter(ind.asString(), result);
			lastFuncCall = ind.asString();
			Value val = null;
			sti.incrNestLevel();
			String res = "function ";
			res = res + ind.asString();
			res = res + " ( ";
			List<DeclarationNode> declList  = node.getDeclrs();
			String	declarationsStr = "";

			declarationsStr += "_scope[0]." + ind.asString() + "Temp" + " = " + "_scopeIndex" + " ; \n";
			declarationsStr += "_scopeIndex++;\n_scope[_scopeIndex] = {};\n";	
			if(declList.size() > 0){
				String idDeclInit = Visit(declList.get(0).getIdNode()).asString();
				res = res + idDeclInit;
				declarationsStr += "_scope[_scopeIndex] = {} ; \n";
				
			if(declList.get(0).getArraySizeExpression() != null){
				//System.out.println(Visit(declList.get(0).getArraySizeExpression()).asInteger());
					switch (declList.get(0).getNodeType()) {
					case SubJSLexer.TYPE_INT:
						int num1[] = {1};
						val = new Value(num1);
						break;
					case SubJSLexer.TYPE_FLOAT:
						double num2[] = {1.1};
						val = new Value(num2);
						break;
					case SubJSLexer.TYPE_STRING:
						String num3[] = {"asr"};
						val = new Value(num3);
						break;
					case SubJSLexer.TYPE_BOOLEAN:
						Boolean num4[] = {true};
						val = new Value(num4);
						break;
					default:
						return null;
					}
			}else{
				switch (declList.get(0).getNodeType()) {
				case SubJSLexer.TYPE_INT:
					int num1 = 1;
					val = new Value(num1);
					break;
				case SubJSLexer.TYPE_FLOAT:
					double num2 = 1.1;
					val = new Value(num2);
					break;
				case SubJSLexer.TYPE_STRING:
					String num3 = "asr";
					val = new Value(num3);
					break;
				case SubJSLexer.TYPE_BOOLEAN:
					Boolean num4 = true;
					val = new Value(num4);
					break;
				default:
					return null;
		        }
			}
		
			
			val.type = declList.get(0).getNodeType();
			sti.enter(Visit(declList.get(0).getIdNode()).asString(), val);
			String idStr =  declList.get(0).getIdNode().getValue().asString();
			declarationsStr += "_scope[0]." + ind.asString() + " = " + idStr + " ; \n";
			declarationsStr += "_scope[_scopeIndex]." + idStr + " = " + idStr + " ; \n";
			for(int i = 1; i< declList.size(); i++){
				idStr =  declList.get(i).getIdNode().getValue().asString();
				declarationsStr += "_scope[_scopeIndex]." + idStr + " = " + idStr + " ; \n";
				if(declList.get(i).getArraySizeExpression() != null ){
					switch (declList.get(i).getNodeType()) {
					case SubJSLexer.TYPE_INT:
						int num1[] = {1};
						val = new Value(num1);
						val.type = SubJSLexer.TYPE_INT;
						break;
					case SubJSLexer.TYPE_FLOAT:
						double num2[] = {1.1};
						val = new Value(num2);
						val.type = SubJSLexer.TYPE_FLOAT;
						break;
					case SubJSLexer.TYPE_STRING:
						String num3[] = {"asr"};
						val = new Value(num3);
						val.type = SubJSLexer.TYPE_STRING;
						break;
					case SubJSLexer.TYPE_BOOLEAN:
						Boolean num4[] = {true};
						val = new Value(num4);
						val.type = SubJSLexer.TYPE_BOOLEAN;
						break;
					default:
						return null;
					}
				}else{
					switch (declList.get(i).getNodeType()) {
					case SubJSLexer.TYPE_INT:
						int num1 = 1;
						val = new Value(num1);
						val.type = SubJSLexer.TYPE_INT;
						break;
					case SubJSLexer.TYPE_FLOAT:
						double num2 = 1.1;
						val = new Value(num2);
						val.type = SubJSLexer.TYPE_FLOAT;
						break;
					case SubJSLexer.TYPE_STRING:
						String num3 = "asr";
						val = new Value(num3);
						val.type = SubJSLexer.TYPE_STRING;
						break;
					case SubJSLexer.TYPE_BOOLEAN:
						Boolean num4 = true;
						val = new Value(num4);
						val.type = SubJSLexer.TYPE_BOOLEAN;
						break;
					default:
						return null;
			        }
				}
				
				val.type = declList.get(i).getNodeType();
				sti.enter(Visit(declList.get(i).getIdNode()).asString(), val);	


				String idDecl = Visit(declList.get(i).getIdNode()).asString();
				String typeDecl = "";// = SubJSLexer.ruleNames[declList.get(i).getNodeType()];
				res = res + " , " + typeDecl + " " + idDecl;
			}
			}
			res = res + " ) ";
			if(node.getBlockNode() != null){
				res +=  "{\n" + declarationsStr + Visit(node.getBlockNode()).getGenCode() +  "}\n";
			}
			val = new Value(null);
			val.setGenCode(res);
			sti.decrNestLevel();
			return val;
		}

		@Override
		public Value Visit(CallFunctExprNode node) {
			Value id = Visit(node.getIdNode());
			String res = id.asString() + " (   ";
			lastFuncCall = id.asString();
			FunctionDeclNode fdn = (FunctionDeclNode)sti.lookup(id.asString()).value;
			List<AbstractASTNode> temppListDecl = new ArrayList<AbstractASTNode>();
			List<AbstractASTNode> temppListAsign = new ArrayList<AbstractASTNode>();
			

			for(int i = 0; i < fdn.getDeclrs().size(); i++){
				DeclarationNode dn =  fdn.getDeclrs().get(i);
				
					switch (dn.getNodeType()) {
				case SubJSLexer.TYPE_INT:
					
					
					if(Visit(node.getArgList().get(i)).isArray()){
						//res = res + ", new _Int(" + ((int[])Visit(node.getArgList().get(i)).value)[Visit(dn.getArraySizeExpression()).asInteger()] + ")";	
						res = res + " " + Visit(node.getArgList().get(i)).getGenCode() + ", ";	
					}else{
						res = res + " new _Int(" + Visit(node.getArgList().get(i)).getGenCode() + "), ";	
					}
					break;
				case SubJSLexer.TYPE_FLOAT:
					if(Visit(node.getArgList().get(i)).isArray()){
						res = res  + " " +  Visit(node.getArgList().get(i)).getGenCode() + ", ";	
					}else{
						res = res + " new _Float(" + Visit(node.getArgList().get(i)).getGenCode() + "), ";
					}
					
					break;
				case SubJSLexer.TYPE_STRING:
					if(Visit(node.getArgList().get(i)).isArray()){
						res = res + " " + Visit(node.getArgList().get(i)).getGenCode() + ", ";	
					}else{
						res = res + " new _String(" + Visit(node.getArgList().get(i)).getGenCode() + "), ";
					}
					break;
				case SubJSLexer.TYPE_BOOLEAN:
					if(Visit(node.getArgList().get(i)).isArray()){
						res = res + " " + Visit(node.getArgList().get(i)).getGenCode() + ", ";	
					}else{
						res = res + " new _Bool(" + Visit(node.getArgList().get(i)).getGenCode() + "), ";
					}
					break;
				default:
					return null;
		        }
				
				
				AssignmentNode an = new AssignmentNode();
				IdNode idn = dn.getIdNode();
				AbstractASTNode nn = node.getArgList().get(i);
				if(dn.getArraySizeExpression() != null){	
					an.setArrayIdIndExprNode(dn.getArraySizeExpression());
				}
				an.setIdNode(idn);
				an.setExprNode(nn);
				temppListDecl.add(dn);
				temppListAsign.add( an);
			}

			Value val = new Value(null);
			val.type = fdn.getNodeType();
			
			//res = "_scopeIndex++;\n_scope[_scopeIndex] = {};\n"  + res;	
			res = removeLast2Char(res);
			res = " " + res +  " );\n";
			//res += "_editScopeAfterReturn();\n";
			val.setGenCode(res);
			return val;
		}

		private static String removeLast2Char(String str) {
	        return str.substring(0,str.length()-2);
	    }
		
		@Override
		public Value Visit(FunctionStatNode node) {
			Value val = Visit(node.getCfen());
			val.setGenCode(val.getGenCode() + " ; ");
			return val;
		}

		public static boolean isDEBUG_MODE() {
			return DEBUG_MODE;
		}

		public static void setDEBUG_MODE(boolean dEBUG_MODE) {
			DEBUG_MODE = dEBUG_MODE;
		}


		@Override
		public Value Visit(ParentExprNode node) {
			 Value value = Visit(node.getInnerNode());
			 return value;
		}

		@Override
		public Value Visit(ReturnStatNode node) {
			Value val = Visit(node.getEn());
			String res = "var result = " + val.getGenCode() + "; \n"; 
			//res += "_scope[0]." + lastFuncCall + "Temp" +  "; \n";
			res += "console.log(_scope[0]." + lastFuncCall + "Temp) ; \n";
			//need to ensure the scope	
			res += "_editScopeAfterReturn(_scope[0]." + lastFuncCall + "Temp);\n";
			res += "return result;";

			val.setGenCode(res);
			return val;
		}



}
