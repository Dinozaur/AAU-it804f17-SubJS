package jss;

import java.util.List;

import jss.SubJSParser.*;
import nodes.*;

public class BuildAstVisitor extends SubJSBaseVisitor<AbstractASTNode>{

	@Override
    public AbstractASTNode visitSubJS(SubJSParser.SubJSContext context){
		return this.visit(context.block());
    }
	
	@Override
    public AbstractASTNode visitBlock(SubJSParser.BlockContext context){
		BlockNode bn = new BlockNode();
		AbstractASTNode en = null;
		for(StatContext sc : context.stat()){
			en = this.visit(sc);
			if(en!=null)
				bn.addStat(en);
		}
		return bn;
    }
	
	@Override
    public AbstractASTNode visitPrint(SubJSParser.PrintContext context) {
		PrintNode pn = new PrintNode();
		AbstractASTNode nn = this.visit(context.expr());
		pn.setInnerNode(nn);	
        return pn;
    }
	
	@Override
    public AbstractASTNode visitParensExpr(SubJSParser.ParensExprContext context) {
		ParentExprNode node = new ParentExprNode();
		AbstractASTNode nn = this.visit(context.expr());
        node.setInnerNode(nn);
        return node;
    }
		
	@Override
    public AbstractASTNode visitDeclaration(SubJSParser.DeclarationContext context)  {
		DeclarationNode node = new DeclarationNode();
		IdNode idNode = new IdNode();
		idNode.setValue(new Value(context.ID().getText()));
		node.setIdNode(idNode);
		  switch (context.type().typ.getType())
	        {
	            case SubJSLexer.TYPE_INT:
	            	node.setNodeType(SubJSLexer.TYPE_INT);
	                break;
	            case SubJSLexer.TYPE_FLOAT:
	            	node.setNodeType(SubJSLexer.TYPE_FLOAT);
	                break;
	            case SubJSLexer.TYPE_STRING:
	            	node.setNodeType(SubJSLexer.TYPE_STRING);
	                break;
	            case SubJSLexer.TYPE_BOOLEAN:
	            	node.setNodeType(SubJSLexer.TYPE_BOOLEAN);
		            break;

	            default:
	            	return null;
	        }
		  if(context.type().typearr() == null){
			  node.setArraySizeExpression(null);
		  }
		  else{
			  if(context.type().typearr().expr() == null){
				  node.setArraySizeExpression(null);
			  }
			  else{
				  AbstractASTNode nn = this.visit(context.type().typearr().expr());	
				  node.setArraySizeExpression(nn);
			  }
		  }
		  
		  if(context.declval == null){
			  node.setExprNode(null);
		  }else{
			  AbstractASTNode nn = this.visit(context.declval);	
		        node.setExprNode(nn);
		  }
			
	return node;
	}
	
	@Override
    public AbstractASTNode visitAssignment(SubJSParser.AssignmentContext context)  {
		AssignmentNode node = new AssignmentNode();
		IdNode idn = new IdNode();
		idn.setValue(new Value(context.ID().getText()));
		AbstractASTNode nn = this.visit(context.esignexpr);	
		
		if(context.ind != null){
			AbstractASTNode aien = this.visit(context.ind);	
	        node.setArrayIdIndExprNode(aien);
		}
		
        node.setIdNode(idn);
        node.setExprNode(nn);
		return node;
    }
	
	@Override
    public AbstractASTNode visitIdAtom(SubJSParser.IdAtomContext context) {
    	IdAtomNode ida = new IdAtomNode();
    	Value val = new Value(context.getText());
    	ida.setValue(val);
        return ida;
    }
	
    @Override
    public AbstractASTNode visitBooleanAtom(SubJSParser.BooleanAtomContext context) {
    	NumberNode nn = new NumberNode();
    	Value v = new Value(Boolean.valueOf(context.getText()));
    	v.type = SubJSLexer.TYPE_BOOLEAN;
    	nn.setValue(v);
        return nn;
    }
	
    @Override
    public AbstractASTNode visitStringAtom(SubJSParser.StringAtomContext context) {
    	NumberNode nn = new NumberNode();
        String str = context.getText();
        str = str.substring(1, str.length() - 1).replace("\"\"", "\"");
    	Value v = new Value(str);
    	v.type = SubJSLexer.TYPE_STRING;
    	nn.setValue(v);
        return nn;
    }
	@Override
    public AbstractASTNode visitFloatAtom(SubJSParser.FloatAtomContext context) {
    	NumberNode nn = new NumberNode();
    	Value v = new Value(Double.parseDouble(context.getText()));
    	v.type = SubJSLexer.TYPE_FLOAT;
    	nn.setValue(v);
        return nn;
    }

	@Override
    public AbstractASTNode visitIntAtom(SubJSParser.IntAtomContext context) {
		NumberNode nn = new NumberNode();
    	Value v = new Value(Integer.parseInt(context.getText()));
    	v.type = SubJSLexer.TYPE_INT;
    	nn.setValue(v);
        return nn;
    }
	@Override
    public AbstractASTNode visitIf_stat(SubJSParser.If_statContext context) {
        List<SubJSParser.Condition_blockContext> conditions =  context.condition_block();
        IfStatNode isn = new IfStatNode();
        for(SubJSParser.Condition_blockContext condition : conditions) {
        	ConditionBlockNode cbn = new ConditionBlockNode();
        	AbstractASTNode en = this.visit(condition.expr());
        	BlockNode sbn = (BlockNode) this.visit(condition.stat_block().block());
            cbn.setExpNode(en);
            cbn.setBlockNode(sbn);
            isn.addConditionBlockNode(cbn);
        }
        if(context.stat_block() != null){
        	BlockNode sbn = (BlockNode) this.visit(context.stat_block().block());
        	isn.setBln(sbn);
        }
        return isn;
    }	
	
	@Override
    public AbstractASTNode visitWhile_stat(SubJSParser.While_statContext context) {
		BlockNode sbn = (BlockNode) this.visit(context.stat_block().block());
		AbstractASTNode en = this.visit(context.expr());
		WhileNode wn = new WhileNode();
		wn.setBln(sbn);
		wn.setExpressionNode(en);
        return wn;
    }	
	
	@Override    
    public  AbstractASTNode visitInfixExpr(SubJSParser.InfixExprContext context) {
        InfixExpressionNode node;
        switch (context.op.getType())
        {
            case SubJSLexer.PLUS:
                node = new AdditionNode();
                break;
            case SubJSLexer.MINUS:
                node = new SubtractionNode();
                break;
            case SubJSLexer.MULT:
                node = new MultiplicationNode();
                break;
            case SubJSLexer.DIV:
                node = new DivisionNode();
                break;
            case SubJSLexer.AND:
                node = new AndNode();
                break;
            case SubJSLexer.OR:
                node = new OrNode();
                break;
            case SubJSLexer.GTEQ:
                node = new GTEQNode();
                break;
            case SubJSLexer.LTEQ:
                node = new LTEQNode();
                break;
            case SubJSLexer.GT:
                node = new GTNode();
                break;
            case SubJSLexer.LT:
                node = new LTNode();
                break;  
            case SubJSLexer.EQ:
                node = new EQNode();
                break;
            case SubJSLexer.NEQ:
                node = new NEQNode();
                break; 
                
            default:
            	return null;
        }
        node.setLeft(this.visit(context.left));
        node.setRight(this.visit(context.right));
        return node;
    } 
	
	@Override  
    public  AbstractASTNode visitUnaryExpr(SubJSParser.UnaryExprContext context){
        switch (context.op.getType())
        {
            case SubJSLexer.PLUS:
                return this.visit(context.expr());
            case SubJSLexer.MINUS:
            	NegateNode nn = new NegateNode();
            	nn.setInnerNode(this.visit(context.expr()));
            	return nn;
            default:
            	return null;
        }
    }
	
	@Override  
    public AbstractASTNode visitArrIndExpr(SubJSParser.ArrIndExprContext context){
		ArrayExprNode aen = new ArrayExprNode();
		IdNode idNode = new IdNode();
		idNode.setValue(new Value(context.ID().getText()));
		aen.setLeft(idNode);
		aen.setRight(this.visit(context.ind));
		return aen;
	}
	
	@Override  
    public AbstractASTNode visitFunctionDecl(SubJSParser.FunctionDeclContext context){
		FunctionDeclNode fdn = new FunctionDeclNode();
		IdNode idNode = new IdNode();
		idNode.setValue(new Value(context.ID().getText()));
		fdn.setIdNode(idNode);
		//add list of declarations of arguments
		BlockNode sbn = (BlockNode) this.visit(context.stat_block().block());
		 fdn.setBln(sbn); 
		switch (context.type().typ.getType()){
			case SubJSLexer.TYPE_INT:
				fdn.setNodeType(SubJSLexer.TYPE_INT);
				break;
	        case SubJSLexer.TYPE_FLOAT:
	        	fdn.setNodeType(SubJSLexer.TYPE_FLOAT);
	        	break;
	        case SubJSLexer.TYPE_STRING:
	        	fdn.setNodeType(SubJSLexer.TYPE_STRING);
	        	break;
	        case SubJSLexer.TYPE_BOOLEAN:
	        	fdn.setNodeType(SubJSLexer.TYPE_BOOLEAN);
	        	break;

	        default:
	        	return null;
	        }
		  if(context.type().typearr() == null){
			  fdn.setArraySizeArray(null);
		  }
		  else{
			  if(context.type().typearr().expr() == null){
				  fdn.setArraySizeArray(null);
			  }
			  else{
				  AbstractASTNode nn = this.visit(context.type().typearr().expr());	
				  fdn.setArraySizeArray(nn);
			  }
		  }
		  if(context.formalParameters() != null){  
		  List<FormalParameterContext> asdd = context.formalParameters().formalParameter();
		  int counter = -1;


		  for(FormalParameterContext exp : asdd) {
			  counter++;
			  DeclarationNode node = new DeclarationNode();
			  IdNode idNodeDecl = new IdNode();
			  idNodeDecl.setValue(new Value(exp.ID().getText()));
			  node.setIdNode(idNodeDecl);
				  switch (exp.type().typ.getType())
			        {
			            case SubJSLexer.TYPE_INT:
			            	node.setNodeType(SubJSLexer.TYPE_INT);
			                break;
			            case SubJSLexer.TYPE_FLOAT:
			            	node.setNodeType(SubJSLexer.TYPE_FLOAT);
			                break;
			            case SubJSLexer.TYPE_STRING:
			            	node.setNodeType(SubJSLexer.TYPE_STRING);
			                break;
			            case SubJSLexer.TYPE_BOOLEAN:
			            	node.setNodeType(SubJSLexer.TYPE_BOOLEAN);
				            break;

			            default:
			            	return null;
			        }
				  if(exp.type().typearr() == null){
					  node.setArraySizeExpression(null);
				  }
				  else{
					  if(exp.type().typearr().expr() == null){
						  NumberNode nn = new NumberNode();
						  Value val = new Value(-1);
						  nn.setValue(val);
						  node.setArraySizeExpression(nn);
					  }
					  else{
						  AbstractASTNode nn = this.visit(exp.type().typearr().expr());	
						  node.setArraySizeExpression(nn);
					  }
				  }
				  fdn.getDeclrs().add(node);
		  }
		  }
	return fdn;
	}
	
	@Override  
    public AbstractASTNode visitFuncExpr(SubJSParser.FuncExprContext context){
		CallFunctExprNode fen = new CallFunctExprNode();
		IdNode idNode = new IdNode();
		idNode.setValue(new Value(context.ID().getText()));
		fen.setIdNode(idNode);
		if(context.exprList() != null){
			List<ExprContext> exprList = context.exprList().expr();
			for(ExprContext exp : exprList) {
				fen.getArgList().add(this.visit(exp));
			}	
		}
		
		return fen;
	}
	
	@Override  
    public AbstractASTNode visitReturn_stat(SubJSParser.Return_statContext context){
		ReturnStatNode rsn = new ReturnStatNode();  //com
		AbstractASTNode en = this.visit(context.expr());
		rsn.setEn(en);  //com
		return rsn;
	}
	
	@Override  
    public AbstractASTNode visitCallfunc(SubJSParser.CallfuncContext context){
		CallFunctExprNode fen = new CallFunctExprNode();
		IdNode idNode = new IdNode();
		idNode.setValue(new Value(context.ID().getText()));
		fen.setIdNode(idNode);
		if(context.exprList() != null){
			List<ExprContext> exprList = context.exprList().expr();
			for(ExprContext exp : exprList) {
				fen.getArgList().add(this.visit(exp));
			}	
		}
		
		//AbstractASTNode nn = this.visit(context.expr());
		FunctionStatNode fsn= new FunctionStatNode();
		fsn.setCfen(fen);
		return fsn;
	}

	
}