bool f(bool arg){
	if(arg){
		log "true!";
	}else{
		log "false!";
	}
	return true;
}

#f(1);  #Faild to assign value of type TYPE_INT to variable arg which has type TYPE_BOOLEAN
#f(0);  #Faild to assign value of type TYPE_INT to variable arg which has type TYPE_BOOLEAN
#f(-1); #Faild to assign value of type TYPE_INT to variable arg which has type TYPE_BOOLEAN

int a = 1;
f(a == 2); # prints false!
f(a = 1); # mismatched input '=' expecting ')'
