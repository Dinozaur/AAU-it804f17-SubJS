#test file used for the small language
#bunch of declarations
float a;
int b;
int c;
bool f;
bool s;
bool p;
bool l;

a = 12.0 - 1.0;
c = 2;
b = 3 + c;
s = true;
p= true;
f = false;
l = f || s;

#print statements
log "s = true: " + s;
log "f = false: " + f;
log "l = f || s: " + l;
log "a = 12.0 - 1.0: " + a;
log "c = 2: " + c;
log "b = 3.3 + c: " + b;

#bunch of conditional statements
if s || f { 
log "IF"; 
}
else if f && s {
log "IF ELSE"; 
} else {
log "ELSE"; 
}

if s && f { 
log "IF"; 
}
else if f || s {
log "IF ELSE"; 
} else {
log "ELSE"; 
}

if s && f { 
log "IF"; 
}
else if f && s {
log "IF ELSE"; 
} else {
log "ELSE"; 
}

if 4 != c {
log "4!=c ";
}
if 2 == c {
log "2==c ";
}
if 4 > c {
log "4 > c ";
}
if 4 >= c {
log "4 > c ";
}
if 1 < c {
log "1 < c ";
}
if 1 <= c {
log "1 <= c ";
}
if s != f {
log "s != f ";
}
if s == true {
log "s == true ";
}
#print next
if s == p {
	log "Level1";
	if 1 == 1 {
		log "Level2";
		if "truee" == "truee" {
			log "Level3";
		}
	}
}
#not print next
if s != p {
	log "Level1";
	if 1 != 1 {
		log "Level2";
		if "truee" != "truee" {
			log "Level3";
		}
	}
}
# print next
if s != f {
	log "Level1";
	if 1 != 2 {
		log "Level2";
		if "truee" != "tru" {
			log "Level3";
		}
	}
}
# not print next
if s == f {
	log "Level1";
	if 1 == 2 {
		log "Level2";
		if "truee" == "tru" {
			log "Level3";
		}
	}
}
# test while here
c = 0;
while c < 6 {
	c = c + 1;
	if c > 3{
		log "cif: " + c;
	}else{
		log "celse: " + c;		
	}
}
#def a function with input param
int f(int x, bool s) {
	c=3;
	return x;
}
#call the function
f(3, true);


int[7] aaa;
int[3] b;
string[3] s;
int c;
aaa[0] = 2;
s[1] = "retert";
b[1] = 5;
c = aaa[0] + b[1];
log "aaa[0]= " + aaa[0];
log "s[1]= " + s[1];
log "c= " + c;

#c = s[1] + aaa[0];
#log "c= " + c;

int step;
step = 1;
if aaa[0] == 2 {
aaa[0] = 0;
while aaa[0] < 6 {
	aaa[0] = aaa[0] + step;
	if aaa[0] > 3{
		log "aaa[0]if: " + aaa[0];
	}else{
		log "aaa[0]else: " + aaa[0];		
	}
}
}
