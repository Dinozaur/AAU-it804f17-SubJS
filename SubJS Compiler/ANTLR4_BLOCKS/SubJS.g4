/**
 * Define a grammar called Sum
 */
grammar SubJS;

// This puts a package statement at the top of the output Java files.
@header {
package jss;
}

// This adds code to the generated parser. Do not change these lines.
@members {
    // This method makes the parser stop running if it encounters
    // invalid input and throw a RuntimeException.
    public void reportErrorsAsExceptions() {
        //removeErrorListeners();
        
        addErrorListener(new ExceptionThrowingErrorListener());
    }
    
    private static class ExceptionThrowingErrorListener extends BaseErrorListener {
        @Override
        public void syntaxError(Recognizer<?, ?> recognizer,
                Object offendingSymbol, int line, int charPositionInLine,
                String msg, RecognitionException e) {
            throw new RuntimeException(msg);
        }
    }
}

/*
 * These are the parser rules. They define the structures used by the parser.
 */
subJS
 : block EOF
 ;

block
 : stat*
 ;
 
stat
 : decl
 | assignment
 | if_stat
 | while_stat
 | print
 | return_stat
 | callfunc
 | OTHER {System.err.println("unknown char: " + $OTHER.text);}
 ;

decl
 : functionDecl
 | declaration
 ;

callfunc
 :   ID '(' exprList? ')' SCOL                         
 ;

print
 : LOG expr SCOL
 ;

return_stat
 : RETURN expr SCOL
 ;
 
declaration
 : type ID ASSIGN declval=expr SCOL
 ;
  
type
 : typ=(TYPE_INT | TYPE_FLOAT | TYPE_STRING | TYPE_BOOLEAN | TYPE_CHAR ) typearr?
 ;

typearr
 : (OSQBRACK expr? CSQBRACK)
 ;

assignment
 : ID ('[' ind=expr ']')? ASSIGN esignexpr=expr SCOL
 ;

if_stat
 : IF condition_block (ELSE IF condition_block)* (ELSE stat_block)?
 ;

condition_block
 : OPAR expr CPAR stat_block
 ;

stat_block
 : OBRACE block CBRACE
 | stat
 ;

while_stat
 : WHILE OPAR expr CPAR stat_block
 ;

expr
    :   ID '(' exprList? ')'                                # funcExpr       // func call like f(), f(x), f(1,2)
	| 	ID '[' ind=expr ']'                          		# arrIndExpr           // array index like a[i], a[i][j]
    |	op=( PLUS | MINUS ) expr                    		# unaryExpr
    |	NOT expr                               				# notExpr
    |   OPAR expr CPAR 										# parensExpr
    |   left=expr op=( MULT | DIV ) right=expr    			# infixExpr
    |   left=expr op=( PLUS | MINUS ) right=expr    		# infixExpr
    | 	left=expr op=( LTEQ | GTEQ | LT | GT ) right=expr 	# infixExpr
    | 	left=expr op=( EQ | NEQ ) right=expr              	# infixExpr
    | 	left=expr op=AND right=expr                			# infixExpr
 	| 	left=expr op=OR right=expr                  		# infixExpr
    |   atom                                     			# nnnumberExpr
    ;
    
exprList 
 : expr ( ',' expr)* // arg list
 ; 
 
atom
 :  INT  			# intAtom
 | FLOAT  			# floatAtom	
 | (TRUE | FALSE) 	# booleanAtom
 | ID             	# idAtom
 | STRING         	# stringAtom
 | CHAR				# charAtom
// | NIL            #nilAtom
 ;
 
functionDecl
: type ID OPAR formalParameters? CPAR stat_block
;
formalParameters
: formalParameter ( ',' formalParameter)*
;
formalParameter
: type ID
;
 
OR : '||';
AND : '&&';
EQ : '==';
NEQ : '!=';
GT : '>';
LT : '<';
GTEQ : '>=';
LTEQ : '<=';
PLUS : '+';
MINUS : '-';
MULT : '*';
DIV : '/';
MOD : '%';
POW : '^';
NOT : '!';

SCOL : ';';
ASSIGN : '=';
OPAR : '(';
CPAR : ')';
OBRACE : '{';
CBRACE : '}';
OSQBRACK : '[';
CSQBRACK : ']';

TRUE : 'true';
FALSE : 'false';
NIL : 'nil';
IF : 'if';
ELSE : 'else';
WHILE : 'while';
LOG : 'print';
RETURN : 'return';
TYPE_INT : 'int' ;
TYPE_FLOAT : 'float' ;
TYPE_STRING : 'string' ;
TYPE_BOOLEAN : 'bool' ;
TYPE_CHAR : 'char';
EXEC : 'exec' ;
ID
 : [a-zA-Z] [a-zA-Z_0-9]*
 ;

INT
 : [0-9]+
 ;

FLOAT
 : [0-9]+ '.' [0-9]* 
 | '.' [0-9]+
 ;

CHAR_LIT: 'a'..'z' | 'A'..'Z' | '0'..'9' | ':' | '.' | '&' | '/' | '\\' | ';';

CHAR
 : '\'' (~["\r\n] | '\'\'') '\''
 ;

STRING
 : '"' (~["\r\n] | '""')* '"'
 ;

COMMENT
 : '#' ~[\r\n]* -> skip
 ;

SPACE
 : [ \t\r\n] -> skip
 ;

OTHER
 : . 
 ;
 

 
WS  :   [ \t\r\n] -> channel(HIDDEN);