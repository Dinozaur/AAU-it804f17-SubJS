// Generated from SubJS.g4 by ANTLR 4.7

package jss;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SubJSParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SubJSVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SubJSParser#subJS}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubJS(SubJSParser.SubJSContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(SubJSParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStat(SubJSParser.StatContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecl(SubJSParser.DeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#callfunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallfunc(SubJSParser.CallfuncContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(SubJSParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#return_stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn_stat(SubJSParser.Return_statContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaration(SubJSParser.DeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(SubJSParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#typearr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypearr(SubJSParser.TypearrContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(SubJSParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#if_stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_stat(SubJSParser.If_statContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#condition_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition_block(SubJSParser.Condition_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#stat_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStat_block(SubJSParser.Stat_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#while_stat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile_stat(SubJSParser.While_statContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrIndExpr}
	 * labeled alternative in {@link SubJSParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrIndExpr(SubJSParser.ArrIndExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code infixExpr}
	 * labeled alternative in {@link SubJSParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInfixExpr(SubJSParser.InfixExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funcExpr}
	 * labeled alternative in {@link SubJSParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncExpr(SubJSParser.FuncExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unaryExpr}
	 * labeled alternative in {@link SubJSParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryExpr(SubJSParser.UnaryExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notExpr}
	 * labeled alternative in {@link SubJSParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotExpr(SubJSParser.NotExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parensExpr}
	 * labeled alternative in {@link SubJSParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParensExpr(SubJSParser.ParensExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nnnumberExpr}
	 * labeled alternative in {@link SubJSParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNnnumberExpr(SubJSParser.NnnumberExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#exprList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprList(SubJSParser.ExprListContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intAtom}
	 * labeled alternative in {@link SubJSParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntAtom(SubJSParser.IntAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code floatAtom}
	 * labeled alternative in {@link SubJSParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloatAtom(SubJSParser.FloatAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code booleanAtom}
	 * labeled alternative in {@link SubJSParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanAtom(SubJSParser.BooleanAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code idAtom}
	 * labeled alternative in {@link SubJSParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdAtom(SubJSParser.IdAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringAtom}
	 * labeled alternative in {@link SubJSParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringAtom(SubJSParser.StringAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code charAtom}
	 * labeled alternative in {@link SubJSParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharAtom(SubJSParser.CharAtomContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#functionDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionDecl(SubJSParser.FunctionDeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#formalParameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormalParameters(SubJSParser.FormalParametersContext ctx);
	/**
	 * Visit a parse tree produced by {@link SubJSParser#formalParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormalParameter(SubJSParser.FormalParameterContext ctx);
}