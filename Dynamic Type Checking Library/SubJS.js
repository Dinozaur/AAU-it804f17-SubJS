/*	
*	This file contains the necessary library functions SubJS
* 	needs for runtime type-checking and code generation in general.
*	Author: Anders Munkgaard
*/

//************ Utility Functions ***************

//Exception object thrown by this file:
function _TypeException(msg) {
	this.msg = msg;
	this.type = "TypeException";
}

var _scope = [];

var _scopeIndex = -1;

function _findScope(varName) {
	var si = this._scopeIndex ;
	var found = false;
	while(!found && si >= 0) {
		found = this._scope[si][varName] !== undefined;
		if(found) {
			break;	
		}
		si--;
	}
	
	if(found) {
		return si;
	}
	else {
		throw new _TypeException("var ".concat(varName).concat(" does not exist!"));
	}	
}
	
function _editScopeAfterReturn(lev) {
	while(this._scopeIndex > lev){
		this._scope[this._scopeIndex]={};
		this._scopeIndex--;
	}
}
	
//************ Type Functions ***************

//string
function _String(arg) {
	if(arg === undefined) {
		//TODO: assume empty string?
		throw new _TypeException("_String() argument was undefined!")
	}

		

	//fields:
	this.type = "string";	//<-- TODO: make this a constant?
	
	//TODO: make sure this.val is always an array of _Char objects..
	this.val = "".concat(arg); //<-- enforcing string
	
	//methods:
	this.length = function() {
		return this.val.length;
	} 
	
	this.concats = function(carg) {
		return new _String(this.val.concat(carg));
	}
	
	this.equal = function(other) {
		
		var retVal = false;
		if(typeof(other) === undefined) {
			throw new _TypeException("_String.equal() argument was undefined!");
		}
		
		if(typeof(other) !== "string") {
			//could be a SubJS string object
			if(other.type === this.type) {
				//other is _String
				if(other.length() === this.length()) {
					var i = 0;
					retVal = true;
					
					while(retVal && i < this.length()) {
						retVal = this.val[i] === other.val[i];
						i++;
					}
				}
				else {
					retval = false;
				}
			}
			else if(other.type === (new _Char()).type) {
				//other is a _Char
				if(this.length === 1) {
					retVal = this.val[0] = other.val;
				}
				else {
					retVal = false;
				}
			}
			else {
				throw new _TypeException("_String.equal() argument was an unexpected type!");
			}
		}
		else {
			//other is a string literal
			if(other.length === this.length()) {
				var i = 0;
				retVal = true; 	//<-- assume they are equal...
				
				//... and check each char for equallity (until a descrepancy is found).
				while(retVal && i < this.length()) {
					retVal = other[i] === this.val[i];
					i++;
				}
			}
			else {
				retVal = false;
			}
		}
		
		return retVal;
	}
}

//int 
function _Int(arg) {
	if(arg === undefined) {
		throw new _TypeException("_Int() argument was undefined!");
	}
	
	if(typeof(arg)!=="number") {
		throw new _TypeException("_Int() argument was not a number!");
	}

	if(_Float.isFloat(arg)) {
		throw new _TypeException("_Int() argument was float!");
	}
	
	//constants
	this.MIN = -1 * Math.pow(2,31);
	this.MAX =  1 * Math.pow(2,31)-1;
	
	if(arg > this.MAX) {
		throw new _TypeException("_Int() argument was to big to be an int!");
	}
	else if(arg < this.MIN) {
		throw new _TypeException("_Int() argument was to small to be an int!");
	}
	
	//fields:
	this.type = "int";
	this.val = arg;
	
	//methods:
	
	// For internal use only! Should not be called outside this file!
	// Performs dynamic type checks and executes opFunc(a,b) 
	// where a = LHS and b = RHS. 
	// Returns an object created through constFunc.
	//
	// opFunc 		is the function manipulating this.val and rhs.
	// rhs 			is the right hand side of the operator. Must be literal int or _Int object.
	// opname 		is the name of the operation, used in exception messages.
	// constFunc	is the constructor function used to create the returned object.
	this.operator = function(opFunc, rhs, opname, constFunc) {
		var resVal; //<-- will contain the [result_object].value
		
		if(typeof(rhs) === undefined) {
			throw new _TypeException("_Int.".concat(opname).concat("() argument was undefined!"));
		}
		
		if(typeof(rhs)!== "number") {
			//if rhs is not a number, then it could be an _Int object:
			if(rhs.type === this.type) {
				//note the assumption that rhs.val is defined.
				resVal = opFunc(this.val, rhs.val);
			}
			else {
				throw new _TypeException("_Int.".concat(opname).concat("() argument was an unexpected object!"));
			}
		}
		else  {
			//rhs is a number literal, but it could still be a float! 
			//if(_Float.isFloat(rhs)) {
				//throw new _TypeException("_Int.".concat(opname).concat("() argument was a float!"));
			//}
			//else {
				resVal = opFunc(this.val, rhs);
			//}
		}
		
		var resObj = new constFunc(resVal);
		
		return resObj;
	} 
	
	//returns the result of adding this.val to:
	// 1) addArg, if addArg is a integer literal
	// 2) addArg.val, if addArg is an _Int object.
	//otherwise throws a _TypeException.
	// The result is encapsulated by an _Int object.
	this.add = function(addArg) {
		var addFunc = function(a,b){
			return a + b;
		}

		if(_Float.isFloat(addArg))	
		{	
			return this.operator(addFunc, addArg, "add", _Float);
		}
		else {
			return this.operator(addFunc, addArg, "add", _Int);
		}
	} 
	
	//copy of _Int.add(), except values are subtracted.
	this.subtract = function(subArg) {
		var subFunc = function(a,b) {
			return a - b;
		}
		if(_Float.isFloat(subArg))	
		{	
			return this.operator(subFunc, subArg, "subtract", _Float);
		}
		else {
			return this.operator(subFunc, subArg, "subtract", _Int);
		}
		
	}

	this.multiply = function(multArg) {
		var mulFunc = function(a,b) {
			return a * b;
		}
		if(_Float.isFloat(multArg))	
		{	
			return this.operator(mulFunc, multArg, "multiply", _Float);
		}
		else {
			return this.operator(mulFunc, multArg, "multiply", _Int);
		}
		
	}
	
	//returnTypeConstructor must say what the return type is
	//this.devide = function(devArg, returnTypeConstructor) {
	
	this.devide = function(devArg){
		var devFuncInt = function(a,b) {
			return _Int.cast(a / b).val;
		}
		var devFuncFloat = function(a,b) {
			return a / b;
		}
		if(_Float.isFloat(devArg))	
		{	
			return this.operator(devFuncFloat, devArg, "devide", _Float);
		}
		else {
			//console.log("0-==---" + _Int.cast(devArg).val);
			return this.operator(devFuncInt, devArg, "devide", _Int);
		}
		
	}
	
	this.lessThan = function(ltArg) {
		var ltFunc = function(a,b) {
			return a < b;
		}
		return this.operator(ltFunc, ltArg, "lessThan", _Bool);
	}
	
	this.lessThanEqual = function(lteArg) {
		var lteFunc = function(a,b) {
			return a <= b;
		}
		
		return this.operator(lteFunc, lteArg, "lessThanEqual", _Bool);
	}
	
	this.greaterThan = function(gtArg) {
		var gtFunc = function(a,b) {
			return a > b;
		}
		return this.operator(gtFunc, gtArg, "greaterThan", _Bool);
	}
	
	this.greaterThanEqual = function(gteArg) {
		var gteFunc = function(a,b) {
			return a >= b;
		}
		
		return this.operator(gteFunc, gteArg, "greaterThanEqual", _Bool);
	}
}

//TODO: should some values coerce to bool? (like 1 = true, 0 = false)..
function _Bool(arg) {
	if(typeof(arg) === undefined) {
		throw new _TypeException("_Bool() argument was undefined!");
	}
	
	if(typeof(arg) !== "boolean") {
		throw new _TypeException("Bool() argument was not boolean!");
	}
	
	//fields:
	this.type = "bool";
	this.val = arg;
	
	//methods:
	this.negate = function() {
		return !(this.val);
	}
}

function _Float(arg) {
	
	if(typeof(arg) === undefined) {
		throw new _TypeException("_Float() argument was undefined!");
	}
	
	if(typeof(arg) !== "number") {
		throw new _TypeException("_Float() argument was not of a number type!");
	}
	
	//fields:
	this.type = "float";
	this.val = arg;
	
	//methods:
	//TODO: define methods
	//methods:
	
	// For internal use only! Should not be called outside this file!
	// Performs dynamic type checks and executes opFunc(a,b) 
	// where a = LHS and b = RHS. 
	// Returns an object created through constFunc.
	//
	// opFunc 		is the function manipulating this.val and rhs.
	// rhs 			is the right hand side of the operator. Must be literal int or _Int object.
	// opname 		is the name of the operation, used in exception messages.
	// constFunc	is the constructor function used to create the returned object.
	this.operator = function(opFunc, rhs, opname, constFunc) {
		var resVal; //<-- will contain the [result_object].value
		
		if(typeof(rhs) === undefined) {
			throw new _TypeException("_Float.".concat(opname).concat("() argument was undefined!"));
		}
		
		if(typeof(rhs)!== "number" && typeof(rhs)!== "int" ) {
			//if rhs is not a number, then it could be an _Int object:
			if(rhs.type === this.type) {
				//note the assumption that rhs.val is defined.
				resVal = opFunc(this.val, rhs.val);
			}
			else {
				throw new _TypeException("_Float.".concat(opname).concat("() argument was an unexpected object!"));
			}
		}
		else  {
			//rhs is a number literal, but it could still be a float! 
			//if(_Float.isFloat(rhs)) {
			//	throw new _TypeException("_Int.".concat(opname).concat("() argument was a float!"));
			//}
			//else {
				resVal = opFunc(this.val, rhs);
			//}
		}
		
		var resObj = new constFunc(resVal);
		
		return resObj;
	} 
	
	//returns the result of adding this.val to:
	// 1) addArg, if addArg is a integer literal
	// 2) addArg.val, if addArg is an _Int object.
	//otherwise throws a _TypeException.
	// The result is encapsulated by an _Int object.
	this.add = function(addArg) {
		var addFunc = function(a,b){
			return a + b;
		}
		return this.operator(addFunc, addArg, "add", _Float);

	} 
	
	//copy of _Int.add(), except values are subtracted.
	this.subtract = function(subArg) {
		var subFunc = function(a,b) {
			return a - b;
		}
		return this.operator(subFunc, subArg, "subtract", _Float);
		
	}

	this.multiply = function(multArg) {
		var mulFunc = function(a,b) {
			return a * b;
		}
		return this.operator(mulFunc, multArg, "multiply", _Float);
		
	}
	
	//returnTypeConstructor must say what the return type is
	this.devide = function(devArg) {
		var devFunc = function(a,b) {
			return a / b;
		}	
		return this.operator(devFunc, devArg, "devide", _Float);

	}
	
	this.lessThan = function(ltArg) {
		var ltFunc = function(a,b) {
			return a < b;
		}
		return this.operator(ltFunc, ltArg, "lessThan", _Bool);
	}
	
	this.lessThanEqual = function(lteArg) {
		var lteFunc = function(a,b) {
			return a <= b;
		}
		
		return this.operator(lteFunc, lteArg, "lessThanEqual", _Bool);
	}
	
	this.greaterThan = function(gtArg) {
		var gtFunc = function(a,b) {
			return a > b;
		}
		
		return this.operator(gtFunc, gtArg, "greaterThan", _Bool);
	}
	
	this.greaterThanEqual = function(gteArg) {
		var gteFunc = function(a,b) {
			return a >= b;
		}
		
		return this.operator(gteFunc, gteArg, "greaterThanEqual", _Bool);
	}
}


function _Char(arg) {
	
	if(typeof(arg) == undefined) {
		throw new _TypeException("_Char() argument was undefined!");
	}
	
	if(typeof(arg) !== "string") {
		//TODO: handle conversion?
		throw new _TypeException("_Char() argument was not a string type!");
	}
	
	if(arg.length != 1) {
		throw new _TypeException("_Char() argument was too long for a single char!");
	}
	
	this.operation
	
	//fields:
	this.type = "char";
	this.val = arg;
	
	// For internal use only! Should not be called outside this file!
	// Performs dynamic type checks and executes opFunc(a,b) 
	// where a = LHS and b = RHS. 
	// Returns an object created through constFunc.
	//
	// opFunc 		is the function manipulating this.val and rhs.
	// rhs 			is the right hand side of the operator. Must be literal char (string of length 1) or _Char object.
	// opname 		is the name of the operation, used in exception messages.
	// constFunc	is the constructor function used to create the returned object.
	this.operator = function(opFunc, rhs, opname, constFunc) {
		var resVal; //<-- will contain the [result_object].value
		
		if(typeof(rhs) === undefined) {
			throw new _TypeException("_Char.".concat(opname).concat("() argument was undefined!"));
		}
		
		if(! _isChar(rhs)) {
			//is rhs an _Char object?
			if(rhs.type === this.type) {
				resVal = opFunc(this.val, rhs.val);
			}
			else {
				throw new _TypeException("_Char".concat(opname).concat("() argument was an unexpected object!"));
			}
		}
		else {
			resVal = opFunc(this.val, rhs);
		}
		
		var resObj = new constFunc(resVal);
		
		return resObj;
	}
	
	//methods:
	
	// creates a string containing this.val followed by concatArg (NOTE: concatArg must be a _Char object)
	this.concat = function(concatArg) {
		var concatFunc = function(a, b) {
			"".concat(a).concat(b);
		}
		
		return this.operator(concatFunc, concatArg, "concat", _String);
	}
}

//************ Static Methods ***************
	
// _isChar checks if arg is of native type string and is of length 1;
// Will not work with SubJS _Char type!
_Char.isChar = function(arg) {
	if(typeof(arg) !== "string") {
		return false;
	}
	
	if(arg.length === 1) {
		return true;
	}
	
	return false;
}

// _isFloat checks to see if arg is a number literal containing a period (e.g. 4.5);
// it will not work with SubJS _Float type.
_Float.isFloat = function(arg) {
	
	if(typeof(arg) !== "number") {
		return false;
	}
	
	//else, let's see if it contains a period:
	var argStr = "".concat(arg);
	if(argStr.match(/^[0-9]+\.[0-9]+$/) !== null) {
		return true;
	}
	
	return false;
}

_Float.cast = function(arg) {
	var retVal = "Something is terribly wrong with _Float.cast()!";
	
	if(typeof(arg) === undefined) {
		
		throw new _TypeException("_Float.cast() argument was undefined!");
	}
	
	if(typeof(arg) !== "number") {
		if(arg.type === (new _Int(0)).type || arg.type === (new _Float(0)).type) {
			retVal = arg.val;
		}
		else {
			throw new _TypeException("_Float.cast() argument was an unexpected type!");
		}
	}
	else {
		retVal = arg;
	}
	
	return new _Float(retVal);
}

_Int.cast = function(arg) {
	var retVal = "Something is not right with _Int.cast()!";
	
	if(typeof(arg) === undefined) {
		throw new _TypeException("_Int.cast() argument was undefined!");
	}
	
	if(typeof(arg) !== "number") {
		if(arg.type === (new _Int(0)).type) {
			retVal = arg.val;
		}
		else {
			throw new _TypeException("_Int.cast() argument was an unexpected type");
		}
	}
	else if(_Float.isFloat(arg)){
		//arg is a number literal, but contains a decimal separator "." 
		var argStr = "".concat(arg);
		var foundSeparator = false;
		var i = 0;
		
		var retValStr = "";
		
		while(!foundSeparator && i < argStr.length) {
			var c = argStr[i];
			if(c === ".") {
				foundSeparator = true;
			}
			else {
				retValStr=retValStr.concat(c);
			}
			
			i++;
		}
		
		retVal = Number(retValStr);
		
	}
	else {
		//arg is a number literal without a decimal separator (an integer literal).
		retVal = arg;
	}
	
	return new _Int(retVal);
}

_String.cast = function(arg) {
	if(arg === undefined) {
		throw new _TypeException("_String.cast() argument was undefined!");
	} 

	if(arg.type === undefined) {
		return new _String("".concat(arg));
	}
	else {
		return new _String("".concat(arg.val));
	}
}




